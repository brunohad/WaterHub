package WaterHub "Water-Energy Nexus library"
  extends WaterHub.Icons.MainIcon;

  package UserGuide "User's Guide"
    extends Modelica.Icons.Information;

    package Contact "Contact"
      extends Modelica.Icons.Information;
      annotation (
        Documentation(info = "<html>
<p><a href=\"https://www.eawag.ch/en/aboutus/portrait/organisation/staff/profile/bruno-hadengue/show/\">Bruno Hadengue</a></p>
<p>Eawag, Swiss Federal Institute of Aquatic Science and Technology,</p>
<p>8600 D&uuml;bendorf, Switzerland </p>
<p><a href=\"mailto:bruno.hadengue@eawag.ch\">bruno.hadengue@eawag.ch</a> </p>
</html>"));
    end Contact;

    package Licence "License"
      extends Modelica.Icons.Information;
      annotation (
        Documentation(info = "<html>
<h4>License</h4>
<p>Modelica Buildings Library. Copyright (c) 1998-2019 Modelica Association, International Building Performance Simulation Association (IBPSA), The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy) and contributors. All rights reserved. </p>
<p>NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so. </p>
<p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: </p>
<ol>
<li>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. </li>
<li>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. </li>
<li>Neither the names of the Modelica Association, International Building Performance Simulation Association (IBPSA), the University of California, Lawrence Berkeley National Laboratory, U.S. Dept. of Energy, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. </li>
</ol>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. </p>
<p>You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades to the features, functionality or performance of the source code (&quot;Enhancements&quot;) to anyone; however, if you choose to make your Enhancements available either publicly, or directly to Lawrence Berkeley National Laboratory, without imposing a separate written license agreement for such Enhancements, then you hereby grant the following license: a non-exclusive, royalty-free perpetual license to install, use, modify, prepare derivative works, incorporate into other computer software, distribute, and sublicense such enhancements or derivative works thereof, in binary and source code form. </p>
<p>Note: The license is a revised 3 clause BSD license with an ADDED paragraph at the end that makes it easy to accept improvements. </p>
</html>"));
    end Licence;

    package Tutorial "A step-by-step tutorial for your first DHW model using the WaterHub Library"
      extends Modelica.Icons.Information;
      annotation (
        Documentation(info="<html>
<p>This tutorial will go through all the steps necessary to build your first DHW model. This tutorial was written for absolute Modelica beginners. If you know about Modelica already, please refer to the examples for more advanced information associated with single models. We have used <a href=\"https://openmodelica.org/download/download-windows\">OpenModelica</a> for our simulation, and all references to the tools or layout used in this tutorial are with respect to that of OpenModelica. For ease of use, we recommend you also use the same.</p>
<p>For a simple demonstration, we will simulate a DHW model comprising a selected few appliances (or end uses): a dishwasher that requires an energy source and cold water input--the water is heated within the system so hot water need not be provided separately; a shower that requires both hot and cold water inputs; a tap that also requires hot and cold water inputs; and a toilet that only uses cold water for flushing.</p>
<p>The hot water is provided using a boiler that uses energy to heat the incoming cold water to a specified temperature. In reality, water (both hot and cold) after use in the appliances are released to an outlet. This is simulated differently in the model which we will deal with later.</p>
<p align=\"center\"><img src=\"modelica://WaterHub/../WH tutorial figures/1. WaterHub tutorial 1.PNG\"/></p>
<p align=\"center\"><b>Fig. 1</b>: An exemplar domestic hot water model comprising a dishwasher, a shower, a tap and a toilet</p>
<h4>Step 1</h4>
<p>Before we begin building up our model, make sure the <a href=\"https://github.com/modelica/ModelicaStandardLibrary\">Modelica Standard Library</a> (MSL) (tested with version 3.2.2) and the <a href=\"https://github.com/lbl-srg/modelica-buildings\">Buildings</a> Library (tested with version 6.0.0) are loaded alongside the WaterHub Library. Once this is done, create a new model and open the &quot;diagram&quot; view.</p>
<h4>Step 2</h4>
<p>The formalism behind the WaterHub Library is that of Material Flow Analysis (MFA), more specifically its extension Material and Energy Flow Analysis (MEFA). In this sense, every &quot;substance&quot; (here water or energy) imported in the system must be either stored or exported outside the boundaries of our system. To model this, WaterHub contains infinite reservoirs or infinite sinks models, in the <i>ImportExport package</i>.</p>
<ul>
<li>For the purpose of this tutorial, we need two import models:<span style=\"font-family: Courier New;\"> ImportExport.Imports.waterSupplyMO</span> and <span style=\"font-family: Courier New;\">ImportExport.Imports.energySupplyMO</span>. &quot;MO&quot; stands for &quot;multiple outputs&quot;. Click on the models and drag them on the left hand side of your blank canvas as shown in <b>Fig. 2</b>. Note that this represents the (cold) water supply and energy supply (&apos;Water in&apos; and &apos;Energy in&apos; in <b>Fig. 1</b>) to the appliances.</li>
<li>We also need two export models:<span style=\"font-family: Courier New;\"> ImportExport.Exports.waterSinkMI</span> and <span style=\"font-family: Courier New;\">ImportExport.Exports.heatSinkMI</span>. &quot;MI&quot; stands for &quot;multiple inputs&quot;. Drag the models on the right hand side of your blank canvas (<b>Fig. 2</b>). This represents the outlet shown in <b>Fig. 1</b>: in the model, the water sink and heat sink are distinguished in accordance with the MEFA formalism of the WaterHub library, where heat sink reflects the heat losses to the environment, and water sink is where used water is released.</li>
<li>You can rename the models in the attributes tab (in OpenModelica) or in the parameters tab (Dymola) with a right-click on the model.We have renamed the models to match those in <b>Fig. 1</b>.</li>
</ul>
<p align=\"center\"><img src=\"modelica://WaterHub/Resources/Images/2. Water and heat source and sink 3.PNG\"/></p>
<p align=\"center\"><br><b>Fig. 2: </b>Water and energy sources and sinks in WaterHub</p>
<h4>Step 3</h4>
<p>We now need to add appliances. We add four appliances of <b>Fig. 1</b> to our domestic hot water system:</p>
<ul>
<li>A &quot;cold-plumbed&quot; appliance (<span style=\"font-family: Courier New;\">Appliances--&gt;ColdPlumbed--&gt;Cold plumbed appliance</span>), meaning an appliance supplied only with cold water and energy. We name it &quot;Dishwasher&quot;.</li>
<li>A shower (<span style=\"font-family: Courier New;\">Appliances--&gt;Showers--&gt;LossShower</span>). We name it &quot;Shower&quot;</li>
<li>A tap (<span style=\"font-family: Courier New;\">Appliances--&gt;Taps--&gt;ClassicTap</span>)</li>
<li>A toilet (<span style=\"font-family: Courier New;\">Appliances--&gt;Toilet</span>). We name it... well, &quot;Toilet&quot;.</li>
</ul>
<p><br>We complete the system by adding a tank boiler (DHWTechnologies--&gt;Boilers--&gt;TankBoiler) (<b>Fig. 3</b>).</p>
<p align=\"center\"><img src=\"modelica://WaterHub/Resources/Images/3. Appliances.PNG\"/></p>
<p align=\"center\"><b>Fig. 3</b>: Inclusion of appliances and a tank boiler in DHW model</p>
<p><br><u>A note on ports:</u> submodels that you drag-and-drop onto the canvas are standalone models. In order to interact with the rest of the system (e.g. the tank boiler supplies the shower with hot water), they need to be connected to other submodels of your DHW model. The purpose of ports is to provide interfaces for interactions, enabling for water and energy to &quot;flow&quot; across the system. We distinguish</p>
<p><b>Water</b> ports: Multiple inputs <img src=\"modelica://WaterHub/Resources/Images/WaterMI.PNG\"/>, single input <img src=\"modelica://WaterHub/Resources/Images/WaterSI.PNG\"/>, multiple outputs <img src=\"modelica://WaterHub/Resources/Images/WaterMO.PNG\"/> and single output <img src=\"modelica://WaterHub/Resources/Images/WaterSO.PNG\"/></p>
<p><b>Energy</b> ports: Multiple inputs<img src=\"modelica://WaterHub/Resources/Images/EnergyMI.PNG\"/>, single input <img src=\"modelica://WaterHub/Resources/Images/EnergySI.PNG\"/>, multiple outputs <img src=\"modelica://WaterHub/Resources/Images/EnergyMO.PNG\"/> and single output <img src=\"modelica://WaterHub/Resources/Images/EnergySO.PNG\"/></p>
<p><br><b>Data</b> ports: inputs <img src=\"modelica://WaterHub/Resources/Images/RealPort.PNG\"/> and outputs <img src=\"modelica://WaterHub/Resources/Images/RealPortOutput_LI.jpg\"/></p>
<p><br>You can connect an output port to an input port of the same type (water, energy or data) by clicking onto them. Multiple ports may be connected with multiple other ports: when you connect to a multiple port, you need to specify which index of the port you would like to connect to: [1], [2], [3], ..., much like choosing the component of a vector. For illustration, we have presented how the energy ports can be connected to the respective appliances in <b>Fig. 4</b>. Remember from<b> Fig. 1</b> that energy needs to be supplied only to two appliances/devices: boiler and dishwasher. As such, the number of ports in the &apos;Energy_in&apos; model needs to be specified as 2 (<b>Fig. 4</b>). Of these, the first outlet is connected to Boiler (hence, index [1]) and the second outlet is connected to Dishwasher (index [2]). </p>
<p align=\"center\"><img src=\"modelica://WaterHub/Resources/Images/4. Energy connection.PNG\"/></p>
<p align=\"center\"><b>Fig. 4</b>: Connecting energy source and appliances</p>
<p>The case for connecting water source to different appliances is similar to connecting energy. But, one important thing to consider for water is that we need to distinguish hot and cold water. This is also shown in <b>Fig. 1: </b>cold water (which is the same as that from source water-- Water_in) is supplied to all five models: the boiler, the dishwasher, the shower, the tap, and the toilet. As such, the number of ports is 5 (<b>Fig. 5</b>). As for the hot water from the boiler, it is supplied to only the shower and the tap. Hence, the number of ports in the boiler is 2 (not shown in <b>Fig. 5</b>). </p>
<p align=\"center\"><br><img src=\"modelica://WaterHub/Resources/Images/5. Water_connection.PNG\"/></p>
<p align=\"center\"><b>Fig. 5</b>: Connecting water source and appliances</p>
<p>Connecting the water and energy ports of the submodels, our DHW system now looks like that shown in <b>Fig. 6</b>.</p>
<p align=\"center\"><br><img src=\"modelica://WaterHub/Resources/Images/6. Final connection 1.PNG\"/></p>
<p align=\"center\"><b>Fig. 6</b>: Overview of the model after connecting all sources, appliances and sinks</p>
<h4>Step 4</h4>
<p>The models created with the WaterHub Modelica Library are demand-triggered: each appliance requires a certain water flow (and temperature) at specific times, depending on the water demand patterns. Model inputs (i.e. water demand) are spreadsheet-like files (.csv) that contain, for each appliance, the time-resolved water flow and temperature. These inputs are <b>data</b>, and will thus be connected to the appliance via their data ports. We will, however, need to add interface models, found under<span style=\"font-family: Courier New;\"> Blocks.Sources.HydrographFromFile</span>. When connecting these models to the <b>data</b> port of the appliances, make sure you connect all the indices of both ports together (connect [:] of one port with [:] of the other port when the dialog pops up). The final model should look something similar to <b>Fig. 7</b>.</p>
<p align=\"center\"><img src=\"modelica://WaterHub/Resources/Images/7. Conn_with_input_1.PNG\"/></p>
<p align=\"center\"><br><b>Fig. 7:</b> Overview of the model with all connections and inputs</p>
<p><br>That&apos;s it, you have essentially built your first DHW system! However, you now need to parametrise the model and specify the inputs.</p>
<h4>Step 5</h4>
<p>By double-clicking on each of the submodels, a dialog box will appear with all parametrisable variables and parameters. In this:</p>
<ul>
<li>You can tune the performance and behavior of the <span style=\"font-family: Courier New;\">TankBoiler</span><b> </b>and <span style=\"font-family: Courier New;\">Shower</span> (loss factors, size, etc.).</li>
<li>In the input models (<span style=\"font-family: Courier New;\">DishesInput</span>,<b> </b><span style=\"font-family: Courier New;\">ShowerInput</span> and <span style=\"font-family: Courier New;\">ToiletInput</span>), we can specify where the input files (.csv) are located on our computed. If you use HydroGen, the stochastic demand generator of the WaterHub Modeling Framework, to generate them, indicate the path to HydroGen&apos;s output files under the parameter &quot;FileName&quot; (you can browse your computer to locate the file by clicking on the [...] button next to the text field (<b>Fig. 8</b>). You can install HydroGen and browse its documentation <a href=\"https://gitlab.com/brunohad/HydroGen2.0\">here</a>. If you don&apos;t use HydroGen yet, you can use .csv files located in the source code of the WaterHub library (under <i>WaterHub/Examples/TestInputs</i>).</li>
</ul>
<p align=\"center\"><br><br><img src=\"modelica://WaterHub/Resources/Images/Step5.PNG\"/></p>
<p align=\"center\"><br><b>Fig. 8:<span style=\"font-family: Helvetica;\"> </span></b>Specifying the input files for each of the appliances </p>
<h4>Step 6</h4>
<p>We can now simulate our DHW system. In order to simulate for one full day, click on &quot;Simulation Setup&quot;, change &quot;Stop Time&quot; to 86400 seconds and click &quot;OK&quot;. This may take a little while. Once the simulation is over, you will be able to browse your results in the Simulation tab of your Modelica Software. You can then plot and explore relevant variables, like <span style=\"font-family: Courier New;\">Shower.inletHot.water</span> (the hot water flow, in L/s, going to the shower from the tank boiler) or <span style=\"font-family: Courier New;\">Dishes.energy_in.heat</span> (the power in Watts being supplied to the dishwasher appliance).</p>
<h4>Conclusions</h4>
<p>Cheers! You have just simulated your first DHW system. You can now go the the <span style=\"font-family: Courier New;\">Examples</span> package and explore some of its models. If you would like to know more about the underlying physics or modeling methodologies of some specific models, check out their associated documentation!</p>
</html>"));
    end Tutorial;
    annotation (
      Documentation(info = "<html>
<p>The <span style=\"font-family: Courier New;\">WaterHub</span> Modelica Library is an open-source library for the dynamic simulation of Domestic Hot Water (DHW) Systems.</p>
<p>The main webpage is <a href=\"https://gitlab.com/brunohad/WaterHub\">https://gitlab.com/brunohad/WaterHub</a>. We note that this library was developed in parallel with the Python tool <span style=\"font-family: Courier New;\">HydroGen </span>(<a href=\"https://gitlab.com/brunohad/HydroGen2.0\">https://gitlab.com/brunohad/HydroGen2.0</a>), which generates stochastic water demand inputs for the WaterHub Library. Any feedback or contribution to the project is welcome.</p>
<h4>Inspiration</h4>
<p>The WaterHub Library was inspired by the Modelica Standard Library and the Buildings Library. Some models even require to load the Buildings Library in parallel. For the rationale behind the WaterHub Library, we refer to the following publications:</p>
<ul>
<li>Hadengue, B., et al. (2019). The WaterHub Modules : Material and Energy Flow Analysis of Domestic Hot Water Systems. 13th International Modelica Conference, March 4-6. Regensburg, Germany, Link&ouml;ping University Press: 639-646.</li>
<li>Hadengue, B., et al. (2020). Modeling the Water-Energy Nexus in Households. Submitted.</li>
</ul>
<h4>Citation</h4>
<p><br>Please cite this library as follows:</p>
<p><br><i>Hadengue, B., et al. (2020). Modeling the Water-Energy Nexus in Households. Submitted.</i></p>
<h4>Links to subguides</h4>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td><p><br><a href=\"modelica://WaterHub.Appliances.UsersGuide\">Appliances</a></p></td>
<td><p>The <i><b>Appliances </b></i>package groups technologies interfacing with the water consumer. Taps, showers, dishwashers, kettles, and WCs are appliances, for instance.</p></td>
</tr>
<tr>
<td><p><br><a href=\"modelica://WaterHub.ImportExport.UsersGuide\">ImportExport</a></p></td>
<td><p>The <i><b>Import/Export</b></i> package groups models simulating imports and exports through the system boundaries. The package contains for instance infinite water/energy sources and sinks. </p></td>
</tr>
<tr>
<td><p><br><br><a href=\"modelica://WaterHub.DHWSystems.UsersGuide\">DHWTechnologies</a></p></td>
<td><p>The <i><b>DHW technologies</b></i> package groups remaining building blocks of a DHW system: e.g., boilers or reservoirs. The package also includes resource-recovering technologies, </p><p>for instance drain water heat exchangers, wastewater-connected heat pumps or decentralized greywater treatment units. </p></td>
</tr>
<tr>
<td><p><br><a href=\"modelica://WaterHub.PipesCarriers.UsersGuide\">Conveyance</a></p></td>
<td><p>The <i><b>Conveyance</b></i> package groups models simulating water and energy transport, for instance water pipes or electric wires. </p></td>
</tr>
<tr>
<td><p><br><a href=\"modelica://WaterHub.Blocks.UsersGuide\">Blocks</a></p></td>
<td><p>The <i><b>Blocks</b></i> package contains utility models: models to interface with the stochastic water demand model as well as compatibility models to allow for the integration of technical </p><p>models outside of the WaterHub Library, for instance models from the <i>Fluid</i> package in the Modelica Buildings Library. </p></td>
</tr>
</table>
</html>"));
  end UserGuide;




















  annotation (
    uses(Buildings(version = "8.0.0"), Modelica(version = "3.2.3"),
    ModelicaServices(version="3.2.3")));
end WaterHub;
