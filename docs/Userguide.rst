Userguide
=========

This section will take you through a step-by-step implementation of your first model using the WaterHub building blocks.


Installation
------------

The WaterHub Modelica package can be loaded as any other Modelica library. The Standard Modelica Library (3.2.2, maybe others) shoud be loaded alongside, as some dependencies are still present. The library is private, as it is still under heavy development, but access will be granted to anybody asking for it (please email bruno.hadengue@eawag.ch). Once you have access to the repository, you can clone the library from GitLab using

::

  git clone https://gitlab.com/brunohad/WaterHub.git

from the terminal (working directory is the location where you want the repository to be cloned to).
