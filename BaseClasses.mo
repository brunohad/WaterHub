within WaterHub;
package BaseClasses "Includes the Base Classes for the different building blocks and the interface behavior (heat, flows)"
  extends Modelica.Icons.BasesPackage;
  import SI = WaterHub.SIUnits;

  connector WaterPort
    flow SI.WaterFlow water;
    SI.AbsoluteTemperature T;
  end WaterPort;

  connector WaterPort_in
    extends WaterPort;
    //, Text(extent={{-150,100},{150,200}},textString="%name")}));
    annotation (
      defaultComponentName = "port_in",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                            FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 127, 255}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                 FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end WaterPort_in;

  connector WaterPort_out
    extends WaterPort;
    //, Text(extent={{-150,100},{150,200}},textString="%name")}));
    annotation (
      defaultComponentName = "port_out",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                            FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 127, 255}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                 FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-80, 80}, {80, -80}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end WaterPort_out;

  connector WaterPorts_in
    extends WaterPort;
    annotation (
      defaultComponentName = "ports_in",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Text(extent = {{-75, 130}, {75, 100}}, textString = "%name"), Rectangle(extent = {{25, -100}, {-25, 100}}, lineColor = {0, 127, 255}), Ellipse(extent = {{-25, 90}, {25, 40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-25, 25}, {25, -25}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-25, -40}, {25, -90}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Rectangle(extent = {{50, -200}, {-50, 200}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 180}, {50, 80}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, -80}, {50, -180}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end WaterPorts_in;

  connector WaterPorts_out
    extends WaterPort;
    annotation (
      defaultComponentName = "ports_b",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Text(extent = {{-75, 130}, {75, 100}}, textString = "%name"), Rectangle(extent = {{-25, 100}, {25, -100}}, lineColor = {0, 0, 0}), Ellipse(extent = {{-25, 90}, {25, 40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-25, 25}, {25, -25}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-25, -40}, {25, -90}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-15, -50}, {15, -80}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-15, 15}, {15, -15}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-15, 50}, {15, 80}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Rectangle(extent = {{-50, 200}, {50, -200}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 180}, {50, 80}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, -80}, {50, -180}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, 100}, {30, 160}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, -100}, {30, -160}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end WaterPorts_out;

  connector WaterPort_inout
    extends WaterPort;
    annotation (
      defaultComponentName = "port_inout",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-50, 50}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                            FillPattern.Solid), Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 127, 255}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                 FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-80, 80}, {80, -80}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 127, 255}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 0, 0}, fillColor = {0, 127, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end WaterPort_inout;

  connector HeatPort
    flow SI.HeatFlow heat;
  end HeatPort;

  connector HeatPort_in
    extends HeatPort;
    annotation (
      defaultComponentName = "port_in",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                           FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {208, 52, 5}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                               FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end HeatPort_in;

  connector HeatPort_out
    extends HeatPort;
    //, Text(extent={{-150,100},{150,200}},textString="%name")}));
    annotation (
      defaultComponentName = "port_out",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                           FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 127, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {208, 52, 5}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                               FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {208, 52, 5},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-80, 80}, {80, -80}}, lineColor = {208, 52, 5}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end HeatPort_out;

  connector HeatPorts_in
    extends HeatPort;
    annotation (
      defaultComponentName = "heatPorts_in",
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-200, -50}, {200, 50}}, initialScale = 0.2), graphics={  Rectangle(extent = {{-201, 50}, {200, -50}}, lineColor = {127, 0, 0}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{-171, 45}, {-83, -45}}, lineColor = {127, 0, 0}, fillColor = {127, 0, 0},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{-45, 45}, {43, -45}}, lineColor = {127, 0, 0}, fillColor = {127, 0, 0},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{82, 45}, {170, -45}}, lineColor = {127, 0, 0}, fillColor = {127, 0, 0},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end HeatPorts_in;

  connector HeatPorts_out
    extends HeatPort;
    annotation (
      defaultComponentName = "heatPorts_b",
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-200, -50}, {200, 50}}, initialScale = 0.2), graphics={  Rectangle(extent = {{-200, 50}, {200, -51}}, lineColor = {127, 0, 0}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{-170, 44}, {-82, -46}}, lineColor = {127, 0, 0}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{-44, 46}, {44, -44}}, lineColor = {127, 0, 0}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Rectangle(extent = {{82, 45}, {170, -45}}, lineColor = {127, 0, 0}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end HeatPorts_out;

  connector AirPort
    flow SI.AirFlow air;
    SI.AbsoluteTemperature T;
  end AirPort;

  connector AirPort_in
    extends AirPort;
    //, Text(extent={{-150,100},{150,200}},textString="%name")}));
    annotation (
      defaultComponentName = "airport_in",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics = {Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 255, 255}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                 FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end AirPort_in;

  connector AirPort_out
    extends AirPort;
    //, Text(extent={{-150,100},{150,200}},textString="%name")}));
    annotation (
      defaultComponentName = "port_out",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics = {Ellipse(extent = {{-40, 40}, {40, -40}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 255, 255}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                 FillPattern.Solid), Ellipse(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-80, 80}, {80, -80}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end AirPort_out;

  connector AirPorts_in
    extends AirPort;
    annotation (
      defaultComponentName = "ports_in",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics = {Text(extent = {{-75, 130}, {75, 100}}, textString = "%name"), Rectangle(extent = {{25, -100}, {-25, 100}}, lineColor = {0, 255, 255}), Ellipse(extent = {{-25, 90}, {25, 40}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-25, 25}, {25, -25}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-25, -40}, {25, -90}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Rectangle(extent = {{50, -200}, {-50, 200}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 180}, {50, 80}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, -80}, {50, -180}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end AirPorts_in;

  connector AirPorts_out
    extends AirPort;
    annotation (
      defaultComponentName = "ports_b",
      Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics = {Text(extent = {{-75, 130}, {75, 100}}, textString = "%name"), Rectangle(extent = {{-25, 100}, {25, -100}}, lineColor = {0, 0, 0}), Ellipse(extent = {{-25, 90}, {25, 40}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-25, 25}, {25, -25}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-25, -40}, {25, -90}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-15, -50}, {15, -80}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-15, 15}, {15, -15}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid), Ellipse(extent = {{-15, 50}, {15, 80}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid)}),
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-50, -200}, {50, 200}}, initialScale = 0.2), graphics={  Rectangle(extent = {{-50, 200}, {50, -200}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 180}, {50, 80}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, 50}, {50, -50}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-50, -80}, {50, -180}}, lineColor = {0, 0, 0}, fillColor = {0, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, 30}, {30, -30}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, 100}, {30, 160}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid), Ellipse(extent = {{-30, -100}, {30, -160}}, lineColor = {0, 255, 255}, fillColor = {255, 255, 255},
              fillPattern =                                                                                                                                                                                                        FillPattern.Solid)}));
  end AirPorts_out;

  package BaseFunctions
    extends Modelica.Icons.Package;
    import SI = WaterHub.SIUnits;
    import CO = WaterHub.Constants;

    function checkIfInArray
      extends Modelica.Icons.Function;
      input Real scal;
      input Real array[:];
      output Boolean bool = false;
    algorithm
      for i loop
        if scal == array[i] then
          bool := true;
        end if;
      end for;
    end checkIfInArray;

    function MediumSwitchWaterAir
      extends Modelica.Icons.Function;
      input Boolean water2air "water 2 air: true, air 2 water: false";
      output CO.Media.generic mediumOutput;
    algorithm
      if water2air then
        mediumOutput.hConv := CO.hConvAir;
        mediumOutput.VolSpecificHeatCap := CO.VolSpecificHeatCapAir;
        return;
      else
        mediumOutput.hConv := CO.hConvWater;
        mediumOutput.VolSpecificHeatCap := CO.VolSpecificHeatCapWater;
        return;
      end if;
    end MediumSwitchWaterAir;

    function waterThermalConductivity "Estimate of thermal conductivity of water"
      extends Modelica.Icons.Function;
      input SI.AbsoluteTemperature T;
      output SI.ThermalConductivity k;
    algorithm
      k := -1e-5*T^2 + 0.0074*T - 0.7522;
      annotation (Documentation(info="<html>
<p>The polynomial interpolation was performed using data from <a href=\"https://www.engineeringtoolbox.com/water-liquid-gas-thermal-conductivity-temperature-pressure-d_2012.html\">here</a>.</p>
<p><br><img src=\"modelica://WaterHub/Resources/Images/ThermalConductivityWater.PNG\"/></p>
</html>"));
    end waterThermalConductivity;

    function waterDynamicViscosity "Estimate of dynamic viscosity of water"
      extends Modelica.Icons.Function;
      input SI.AbsoluteTemperature T;
      output SI.DynamicViscosity mu;
    algorithm
      mu := 3e-11*(T-273.15)^4 - 9e-9*(T-273.15)^3 + 1e-6*(T-273.15)^2 - 6e-5*(T-273.15) + 0.0018;
      annotation (Documentation(info="<html>
<p>The polynomial interpolation was performed using data from <a href=\"https://www.engineeringtoolbox.com/water-dynamic-kinematic-viscosity-d_596.html\">here</a></p>
<p><br><img src=\"modelica://WaterHub/Resources/Images/DynamicViscosityWater.PNG\"/></p>
</html>"));
    end waterDynamicViscosity;



    function waterPrandtlNumber "Estimate of Prandtl number for water"
      extends Modelica.Icons.Function;
      input SI.AbsoluteTemperature T;
      output Real P;
    algorithm
      P := waterDynamicViscosity(T)*CO.VolSpecificHeatCapWater/
        waterThermalConductivity(T);
      annotation (Documentation(info="<html>
<p>The Prandtl number is computed using the following relation</p>
<p><br><img src=\"modelica://WaterHub/Resources/Images/equations/equation-7p7eqc2r.png\" alt=\"Pr = mu * c_w / k\"/></p>
<p><br>where <img src=\"modelica://WaterHub/Resources/Images/equations/equation-86MyBCfF.png\" alt=\"mu\"/> is the dynamic viscosity, <img src=\"modelica://WaterHub/Resources/Images/equations/equation-JqVJBIpK.png\" alt=\"c_w\"/> the specific heat capacity and <img src=\"modelica://WaterHub/Resources/Images/equations/equation-lHgerBBu.png\" alt=\"k\"/> the thermal conductivity of water.</p>
</html>"));
    end waterPrandtlNumber;
  end BaseFunctions;
end BaseClasses;
