within WaterHub.ImportExport;
package Imports
  extends Modelica.Icons.Package;

  model WaterSupplySO "Infinite Supply of water, basically just one outlet port that delivers whatever flow is needed"
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_out outlet
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
    parameter SI.AbsoluteTemperature T=283 "Temperature of supplied water flow";
  equation
    outlet.T = T;
    annotation (Icon(graphics={Bitmap(extent={{-80,-66},{78,88}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/Water.bmp")}));
  end WaterSupplySO;

  model WaterSupplyMO "Infinite Supply of water, basically several outlet ports that deliver whatever flow is needed"
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    WaterHub.BaseClasses.WaterPorts_out[n] outlets
  annotation (Placement(transformation(extent={{90,-40},{110,40}})));
    parameter SI.AbsoluteTemperature T=283 "Temperature of supplied water flow";
  equation
    for i in 1:n loop
      outlets[i].T = T;
    end for;
    annotation (Icon(graphics={Bitmap(extent={{-80,-66},{78,88}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/Water.bmp")}));
  end WaterSupplyMO;


  model EnergySupplySO
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.HeatPort_out outlet annotation (Placement(transformation(extent={{90,-10},{110,10}})));
    annotation (Icon(graphics={Bitmap(extent={{-80,-66},{78,88}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/Energy.bmp")}));
  end EnergySupplySO;

  model EnergySupplyMO
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    WaterHub.BaseClasses.HeatPorts_out[n] outlet annotation (Placement(transformation(extent={{90,-40},{110,40}})));
    annotation (Icon(graphics={Bitmap(extent={{-80,-66},{78,88}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/Energy.bmp")}));
  end EnergySupplyMO;




end Imports;
