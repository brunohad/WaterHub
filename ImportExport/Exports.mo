within WaterHub.ImportExport;
package Exports
  extends Modelica.Icons.Package;

  partial model BaseWaterSink
    //extends Modelica.Icons.BasesPackage;
    SI.Volume cumulatedWater(start=0.0);
  //    SI.AbsoluteTemperature cumulatedWaterT(start=283);
    annotation (Icon(graphics={Rectangle(
            extent={{-100,100},{100,-100}},
            lineThickness=0.5,
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid,
            pattern=LinePattern.None), Bitmap(extent={{-86,-80},{80,82}},
              fileName="modelica://WaterHub/Resources/Images/Icons/Export.png")}));
  end BaseWaterSink;

  model WaterSinkSI "Endless water sink with single inlet"
    extends BaseWaterSink;
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_in inlet
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  equation
    der(cumulatedWater) = inlet.water;
  end WaterSinkSI;

  model WaterSinkMI "Endless water sink with multiple inlets"
    extends BaseWaterSink;
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    final parameter SI.AbsoluteTemperature referenceT = 273.15;
    final constant Real triggerValue = 0.001;
    Boolean event(start = false);
    SI.AbsoluteTemperature cumulatedWaterT(start=referenceT);
    WaterHub.BaseClasses.WaterPorts_in[n] inlet
    annotation (Placement(transformation(extent={{-110,-40},{-90,40}})));

  equation
    der(cumulatedWater) = sum(inlet[i].water for i in 1:n);

  // heat balance: the if-statement is required to avoid unsolvable equation when incoming flow = 0;
    event = der(cumulatedWater) > triggerValue "Careful, as a too high triggerValue leads to Chattering problems";

    if event then
      sum(inlet[i].water * inlet[i].T for i in 1:n) = der(cumulatedWater) * cumulatedWaterT "heat balance";
    else
      cumulatedWaterT = referenceT;
    end if;

    annotation (
      Documentation(info="<html>
<p><br>Infinite water sink with multiple inlets. The model computes the average temperature of all incoming streams using the heat balance</p>
<p><br><span style=\"font-size: 12pt;\"><img src=\"modelica://WaterHub/Resources/Images/equations/equation-Dt3fwRCw.png\" alt=\"sum(Q_i*T_i) = Q_tot * T_tot\"/></span></p>
<p>Note that when Qtot is zero, cumulatedWaterT is set to the reference temperature, i.e. 273.15 K.&amp;nbsp</p>
</html>"));
  end WaterSinkMI;



















  partial model BaseHeatSink
    //extends Modelica.Icons.BasesPackage;
    SI.Heat cumulatedHeat(start=0.0);
    annotation (Icon(graphics={
          Line(
            points={{-60,-80},{-38,-38},{-80,0},{-42,42},{-56,80}},
            color={255,0,0},
            smooth=Smooth.Bezier,
            thickness=0.5),
          Line(
            points={{-20,-80},{2,-38},{-40,0},{-2,42},{-16,80}},
            color={255,0,0},
            smooth=Smooth.Bezier,
            thickness=0.5),
          Line(
            points={{20,-80},{42,-38},{0,0},{38,42},{24,80}},
            color={255,0,0},
            smooth=Smooth.Bezier,
            thickness=0.5),
          Line(
            points={{60,-80},{82,-38},{40,0},{78,42},{64,80}},
            color={255,0,0},
            smooth=Smooth.Bezier,
            thickness=0.5)}));
  end BaseHeatSink;

  model HeatSinkSI "Endless heat sink with single inlet"
    extends BaseHeatSink;
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.HeatPort_in inlet
    annotation (Placement(transformation(extent={{-110,-40},{-90,40}})));
  equation
    der(cumulatedHeat) = inlet.heat;
  end HeatSinkSI;

  model HeatSinkMI "Endless heat sink with multiple inlet"
    extends BaseHeatSink;
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    WaterHub.BaseClasses.HeatPorts_in[n] inlet
    annotation (Placement(transformation(extent={{-110,-40},{-90,40}})));
    SI.Heat[n] partialCumulatedHeat(each start=0.0);
  equation
    for i in 1:n loop
  der(partialCumulatedHeat[i]) = inlet[i].heat;
    end for;

    der(cumulatedHeat) = sum(inlet[i].heat for i in 1:n);
  end HeatSinkMI;

end Exports;
