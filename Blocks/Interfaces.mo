within WaterHub.Blocks;
package Interfaces
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  extends Modelica.Icons.Package;

  model MSL2WaterHub_WaterSISO
    extends WaterHub.Icons.ModelIcon;
    BaseClasses.WaterPort_out port_out annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  //  Modelica.Fluid.Interfaces.FluidPort_a port_in(replaceable package Medium =
  //        Modelica.Media.Water.ConstantPropertyLiquidWater) annotation (
  ////    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Fluid.Interfaces.FluidPort_a port_in(redeclare package Medium =
          Buildings.Media.Water) annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

  equation
    port_in.m_flow + port_out.water = 0 "flow balance";
    port_in.h_outflow = inStream(port_in.h_outflow) "set enthalpy to enthalpy of incoming flow";
    port_out.T = port_in.h_outflow / Modelica.Media.Water.ConstantPropertyLiquidWater.cv_const + 273.15 "T from specific enthalpy";
  end MSL2WaterHub_WaterSISO;

  model MSL2WaterHub_WaterSIMO
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n = 1 "Number of outlet ports";
    BaseClasses.WaterPorts_out[n] port_out annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {99, 0}, extent = {{-11, -36}, {11, 36}}, rotation = 0)));
    Modelica.Fluid.Interfaces.FluidPort_a port_in(replaceable package Medium =
          Modelica.Media.Water.ConstantPropertyLiquidWater)                                                                      annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    port_in.m_flow + sum(port_out.water) = 0 "flow balance";
    port_in.h_outflow = inStream(port_in.h_outflow) "set enthalpy to enthalpy of incoming flow";
    for i in 1:n loop
      port_out[i].T = port_in.h_outflow / Modelica.Media.Water.ConstantPropertyLiquidWater.cv_const + 273.15 "T from specific enthalpy";
    end for;
  end MSL2WaterHub_WaterSIMO;

  model WaterHub2MSL_WaterSISO
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_in port_in annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Fluid.Interfaces.FluidPort_b port_out(replaceable package Medium =
          Modelica.Media.Water.ConstantPropertyLiquidWater)                                                                       annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    port_in.water + port_out.m_flow = 0 "flow balance";
    //  port_out.h_outflow = actualStream(port_out.h_outflow) "set enthalpy to enthalpy of incoming flow";
    port_in.T - 273.15 = port_out.h_outflow / Modelica.Media.Water.ConstantPropertyLiquidWater.cv_const "Specific enthalpy from T";
    // partial concentrations are not considered: we have pure water
    // port_out.p = Modelica.Media.Water.ConstantPropertyLiquidWater.p_default;
    for i in 1:Modelica.Media.Water.ConstantPropertyLiquidWater.nXi loop
      port_out.Xi_outflow[i] = 0;
    end for;
    for i in 1:Modelica.Media.Water.ConstantPropertyLiquidWater.nC loop
      port_out.Ci_outflow[i] = 0;
    end for;
  end WaterHub2MSL_WaterSISO;

  package Examples
    extends Modelica.Icons.ExamplesPackage;

    model MSL2WaterHubExample
      extends Modelica.Icons.Example;
      Modelica.Fluid.Sources.FixedBoundary boundary(replaceable package Medium =
            Modelica.Media.Water.ConstantPropertyLiquidWater,                                                                      T = 273.15 + 30, nPorts = 1) annotation (
        Placement(visible = true, transformation(origin = {-116, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_Water1 annotation (
        Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Toilet toilet1 annotation (
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile1(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/Toilet_tmp.csv") annotation (
        Placement(visible = true, transformation(origin = {-16, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_Water1 annotation (
        Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Fluid.Sources.FixedBoundary boundary1(replaceable package Medium =
            Modelica.Media.Water.ConstantPropertyLiquidWater,                                                                       nPorts = 1) annotation (
        Placement(visible = true, transformation(origin = {120, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
    equation
      connect(waterHub2MSL_Water1.port_out, boundary1.ports[1]) annotation (
        Line(points = {{80, 0}, {110, 0}, {110, 0}, {110, 0}}, color = {0, 127, 255}));
      connect(toilet1.outlet, waterHub2MSL_Water1.port_in) annotation (
        Line(points = {{10, 0}, {60, 0}}, color = {0, 127, 255}));
      connect(hydrographFromFile1.y, toilet1.flowInput) annotation (
        Line(points={{-5,26},{-5,25},{0,25},{0,17.5},{0,9.6},{0,9.6}},                                 color = {0, 0, 127}, thickness = 0.5));
      connect(mSL2WaterHub_Water1.port_out, toilet1.inlet) annotation (
        Line(points = {{-60, 0}, {-10, 0}}, color = {0, 127, 255}));
      connect(boundary.ports[1], mSL2WaterHub_Water1.port_in) annotation (
        Line(points = {{-106, 0}, {-80, 0}}, color = {0, 127, 255}, thickness = 0.5));
      annotation (Documentation(info="<html>
<p>Documentation of this example will soon follow.</p>
</html>"));
    end MSL2WaterHubExample;

    model WaterHub2MSLExample
      extends Modelica.Icons.Example;
      package Medium = Buildings.Media.Water "Medium model";
      Buildings.Fluid.HeatExchangers.Heater_T hea(redeclare package Medium = Medium, dp_nominal = 100, m_flow_nominal = 0.2) annotation (
        Placement(visible = true, transformation(origin = {10, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.WaterSupplySO waterSupplySO annotation (
        Placement(visible = true, transformation(origin = {-116, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_Water2 annotation (
        Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_Water2 annotation (
        Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkSI waterSinkSI annotation (
        Placement(visible = true, transformation(origin = {116, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Constant const(k = 330) annotation (
        Placement(visible = true, transformation(origin = {-7, 31}, extent = {{-7, -7}, {7, 7}}, rotation = -90)));
      WaterHub.Appliances.Toilet toilet1 annotation (
        Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile1(fileName = "C:/Users/hadengbr/polybox/EAWAG/05_Raw_Data/05_06_Validation/190710_Validation3Eflow_Reloaded/tmp/181016_Validation3Eflow_M.csv") annotation (
        Placement(visible = true, transformation(origin = {-88, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(mSL2WaterHub_Water2.port_out, waterSinkSI.inlet) annotation (
        Line(points = {{80, 0}, {106, 0}}, color = {0, 127, 255}));
      connect(const.y, hea.TSet) annotation (
        Line(points = {{-7, 23.3}, {-8, 23.3}, {-8, 8}, {-2, 8}}, color = {0, 0, 127}));
      connect(hea.port_b, mSL2WaterHub_Water2.port_in) annotation (
        Line(points = {{20, 0}, {60, 0}}, color = {0, 127, 255}));
      connect(waterHub2MSL_Water2.port_out, hea.port_a) annotation (
        Line(points = {{-20, 0}, {0, 0}}, color = {0, 127, 255}));
      connect(toilet1.outlet, waterHub2MSL_Water2.port_in) annotation (
        Line(points = {{-60, 0}, {-40, 0}}, color = {0, 127, 255}));
      connect(waterSupplySO.outlet, toilet1.inlet) annotation (
        Line(points = {{-106, 0}, {-82, 0}, {-82, 0}, {-80, 0}}, color = {0, 127, 255}));
      connect(hydrographFromFile1.y, toilet1.flowInput) annotation (
        Line(points={{-77,30},{-70,30},{-70,9.6},{-70,9.6}},                            color = {0, 0, 127}, thickness = 0.5));
      annotation (Documentation(info="<html>
<p>Documentation of this example will soon follow.</p>
</html>"));
    end WaterHub2MSLExample;
  end Examples;
end Interfaces;
