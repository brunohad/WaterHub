within WaterHub.DHWTechnologies;
package Reservoirs
  extends Modelica.Icons.Package;

  partial model BaseWaterReservoir
    extends Modelica.Icons.BasesPackage;
    SI.Volume volume(start=0.0);
  end BaseWaterReservoir;

  model WaterReservoirSI "Ideal Water Reservoir with single inlet-outlet port"
    extends BaseWaterReservoir;
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_inout inlet
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  equation
    der(volume) - inlet.water = 0;
  end WaterReservoirSI;

  model WaterReservoirMI "Endless water sink with multiple inlets-outlets ports"
    extends BaseWaterReservoir;
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    WaterHub.BaseClasses.WaterPorts_inout[n] inlet
    annotation (Placement(transformation(extent={{-110,-40},{-90,40}})));
  //  SI.AbsoluteTemperature AvgTemp(start=283);
  equation
    der(volume) - sum(inlet[i].water for i in 1:n) = 0;
  //  if der(cumulatedWater) > 1e-1 then
  //    AvgTemp = inlet.water*inlet.T/sum(inlet.water);
  //  else
  //    AvgTemp = pre(AvgTemp);
  //  end if;
  end WaterReservoirMI;

end Reservoirs;
