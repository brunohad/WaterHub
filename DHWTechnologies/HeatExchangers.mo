within WaterHub.DHWTechnologies;
package HeatExchangers
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  extends Modelica.Icons.Package;

  model SimpleHeatExchanger "Retrieves energy and direct input into SimpleShower"
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_in inlet;
    WaterHub.BaseClasses.WaterPort_out outlet;
    WaterHub.BaseClasses.HeatPort_out heatOutlet;
    parameter Real efficiency = 0.15 "Efficiency of heat retrieval";
    SI.HeatFlow heat_in;
  equation
    inlet.water + outlet.water = 0;
    heat_in = CO.VolSpecificHeatCapWater * inlet.T * inlet.water;
    -heatOutlet.heat = efficiency * heat_in;
    outlet.T = (1 - efficiency) * heat_in / (outlet.water * CO.VolSpecificHeatCapWater);
  end SimpleHeatExchanger;

  model NotSoSimpleHeatExchanger "Derived from https://en.wikipedia.org/wiki/Heat_exchanger : model of simple heat exchanger"
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.WaterPort_in inletHot(water(min = -10e-5)) annotation (
      Placement(transformation(extent = {{-10, 90}, {10, 110}})));
    WaterHub.BaseClasses.WaterPort_in inletCold(water(min = -10e-5)) annotation (
      Placement(transformation(extent = {{-10, -110}, {10, -90}})));
    WaterHub.BaseClasses.WaterPort_out outletHot(water(max = 10e-5)) annotation (
      Placement(transformation(extent = {{90, -10}, {110, 10}})));
    WaterHub.BaseClasses.WaterPort_out outletCold(water(max = 10e-5)) annotation (
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //Real alphaFactor(start=0.3, max=1.0) "Efficiency factor. Depends on length of tube, heat exchange coefficient, flows and area of contact";
    parameter Real alphaFactor = 0.3 "Efficiency factor. Depends on length of tube, heat exchange coefficient, flows and area of contact";
    Boolean event(start = false);
    parameter Real flowHE = 1 "1 if parallel flow, -1 if counterflow";
    SI.TemperatureDifference dT "Temperature between hot/cold inputs";
  equation
    //der(alphaFactor)=0.00001;
    dT = inletHot.T - inletCold.T;
    //cannot be negative! No big consequence be it that the heat flow goes the other way!
    outletCold.water + inletCold.water = 0;
    outletHot.water + inletHot.water = 0;
    event = inletHot.water > 0.01 and inletCold.water > 0.01;
    if event then
      outletCold.T = inletCold.T + dT / (1 + flowHE * abs(inletCold.water / inletHot.water)) * alphaFactor "Expression for preheated cold water";
      outletHot.T = inletHot.T - dT / (1 + flowHE * abs(inletHot.water / inletCold.water)) * alphaFactor "Expression for outlet hot water";
    else
      outletCold.T = inletCold.T;
      outletHot.T = inletHot.T;
    end if;
    //    inletCold.T*inletCold.water + inletHot.T*inletHot.water =
    //    -(outletCold.T*outletCold.water+outletHot.T*outletHot.water) "energy balance";
    annotation (
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Line(points = {{-92.4, 92.4}, {92.4, -92.4}}, color = {0, 0, 0}, thickness = 0.5), Line(points = {{0, -100}, {-5, -5}, {-100, 0}}, color = {0, 0, 255}, thickness = 1.5, smooth = Smooth.Bezier), Line(points = {{0, 100}, {5, 5}, {100, 0}}, color = {208, 52, 43}, thickness = 1.5, smooth = Smooth.Bezier)}));
  end NotSoSimpleHeatExchanger;

model BuildingsDryCoilCounterFlow
  extends WaterHub.Icons.ModelIcon;
  package Medium = Buildings.Media.Water "Medium model";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.2 "Nominal mass flow rate";
parameter Modelica.SIunits.Temperature T_inletCold_nominal=5 + 273.15
  "Nominal cold inlet temperature";
parameter Modelica.SIunits.Temperature T_outletCold_nominal=10 + 273.15
  "Nominal cold outlet temperature";
parameter Modelica.SIunits.Temperature T_inletHot_nominal=30 + 273.15
  "Nominal hot inlet temperature";
parameter Modelica.SIunits.Temperature T_outletHot_nominal=15 + 273.15
  "Nominal hot outlet temperature";
parameter Modelica.SIunits.HeatFlowRate Q_flow_nominal = m_flow_nominal*4200*(T_inletCold_nominal-T_outletCold_nominal)
  "Nominal heat transfer";
  Buildings.Fluid.HeatExchangers.DryCoilCounterFlow heaCoi(redeclare package
        Medium1 =                                                                      Medium, redeclare
        package Medium2 =                                                                                                  Medium, UA_nominal=Q_flow_nominal/Buildings.Fluid.HeatExchangers.BaseClasses.lmtd(
        T_inletCold_nominal,
        T_outletCold_nominal,
        T_inletHot_nominal,
        T_outletHot_nominal), airSideFlowDependent = false, dp1_nominal = 3000, dp2_nominal = 200, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m1_flow_nominal = m_flow_nominal, m1_flow_small = 1E-4 * abs(m_flow_nominal), m2_flow_nominal = m_flow_nominal, m2_flow_small = 1E-4 * abs(m_flow_nominal), nEle = 5, r_nominal = 1, show_T = false, tau1 = 1, tau2 = 1, waterSideFlowDependent = false) annotation (
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.WaterPort_in cold_port_in annotation (
      Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.WaterPort_out cold_port_out annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.BaseClasses.WaterPort_in hot_port_in annotation (
      Placement(visible = true, transformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.WaterPort_out hot_port_out annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_Water1
      annotation (Placement(visible=true, transformation(
          origin={0,64},
          extent={{-10,-10},{10,10}},
          rotation=-90)));
    Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_Water1 annotation (
        Placement(visible=true, transformation(
          origin={60,6},
          extent={{-10,-10},{10,10}},
          rotation=0)));
    WaterHub.Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_Water2
      annotation (Placement(visible=true, transformation(
          origin={-62,-6},
          extent={{10,-10},{-10,10}},
          rotation=0)));
    WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_Water2
      annotation (Placement(visible=true, transformation(
          origin={0,-54},
          extent={{-10,-10},{10,10}},
          rotation=90)));
equation
    connect(mSL2WaterHub_Water2.port_out, cold_port_out) annotation (
      Line(points = {{-72, -6}, {-82, -6}, {-82, 0}, {-98, 0}, {-98, 0}, {-100, 0}}, color = {0, 127, 255}));
    connect(heaCoi.port_b2, mSL2WaterHub_Water2.port_in) annotation (
      Line(points = {{-10, -6}, {-52, -6}}, color = {0, 127, 255}));
    connect(waterHub2MSL_Water2.port_out, heaCoi.port_a2) annotation (
      Line(points = {{0, -44}, {0, -44}, {0, -28}, {34, -28}, {34, -6}, {10, -6}, {10, -6}}, color = {0, 127, 255}));
    connect(cold_port_in, waterHub2MSL_Water2.port_in) annotation (
      Line(points = {{0, -100}, {0, -100}, {0, -64}, {0, -64}}, color = {0, 127, 255}));
    connect(mSL2WaterHub_Water1.port_out, hot_port_out) annotation (
      Line(points = {{70, 6}, {78, 6}, {78, 0}, {100, 0}, {100, 0}}, color = {0, 127, 255}));
    connect(heaCoi.port_b1, mSL2WaterHub_Water1.port_in) annotation (
      Line(points = {{10, 6}, {50, 6}, {50, 6}, {50, 6}}, color = {0, 127, 255}));
    connect(waterHub2MSL_Water1.port_out, heaCoi.port_a1) annotation (
      Line(points = {{0, 54}, {0, 54}, {0, 34}, {-34, 34}, {-34, 6}, {-10, 6}, {-10, 6}}, color = {0, 127, 255}));
    connect(hot_port_in, waterHub2MSL_Water1.port_in) annotation (
      Line(points = {{0, 100}, {0, 100}, {0, 74}, {0, 74}}, color = {0, 127, 255}));
    annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Line(points = {{-92.4, 92.4}, {92.4, -92.4}}, color = {0, 0, 0}, thickness = 0.5), Line(points = {{0, -100}, {-5, -5}, {-100, 0}}, color = {0, 0, 255}, thickness = 1.5, smooth = Smooth.Bezier), Line(points = {{0, 100}, {5, 5}, {100, 0}}, color = {208, 52, 43}, thickness = 1.5, smooth = Smooth.Bezier)}));
end BuildingsDryCoilCounterFlow;

  model DiscretizedHEX "Model of a heat exchanger based on the heat transfer between virtual nodes. Default is counterflow: the cold inlet is in contact with the hot outlet, and vice versa. NOTE: the model is not in contact with the environement, i.e. no contained energy is lost inbetween events. As a consequence, the temperatures dynamics is a little bit weird, as an equilibrium is found between all nodes."
    extends WaterHub.Icons.ModelIcon;

    //inlets
    WaterHub.BaseClasses.WaterPort_in inletHot(water(min = -1e-5)) annotation (
      Placement(transformation(extent = {{-10, 90}, {10, 110}})));
    WaterHub.BaseClasses.WaterPort_in inletCold(water(min = -1e-5)) annotation (
      Placement(transformation(extent = {{-10, -90}, {10, -110}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outletHot(water(max = 1e-5)) annotation (
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.WaterPort_out outletCold(water(max = 1e-5)) annotation (
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet(heat(start=0.0)) "heat outlet for thermal losses. Dummy if thermalLosses = false" annotation (
      Placement(transformation(extent = {{40, -60}, {60, -40}})));

    //parameters
    parameter Integer nbNodes = 1 "Number of nodes";
    parameter Boolean counterFlow = true;
    parameter CO.MaterialConstants.Material material = CO.MaterialConstants.Material.Copper;
    parameter SI.Volume volumeHotFlow = m_flow_nominal*nbNodes/10 "leads to numerical instability if too high";
    parameter SI.Volume volumeColdFlow = m_flow_nominal*nbNodes/10 "leads to numerical instability if too high";
    parameter SI.Volume volumeWall = 0.02;
    parameter Modelica.SIunits.Temperature tEnvironment = 283.15;
    parameter Boolean thermalLosses = true;
    SI.Volume nodeVolumeHotFlow[nbNodes];
    SI.Volume nodeVolumeColdFlow[nbNodes];
    SI.Volume nodeVolumeWall[nbNodes];
    SI.AbsoluteTemperature nodeHotFlowT[nbNodes](each start = tEnvironment);
    SI.AbsoluteTemperature nodeColdFlowT[nbNodes](each start = tEnvironment);
    SI.AbsoluteTemperature nodeWallT[nbNodes](each start = tEnvironment);
    SI.ThermalConductivity k;
    SI.Density wallDensity;
    SI.VolSpecificHeatCap CWall;
    SI.OverallConductance UAnominal;
    SI.HeatFlow nodeHeatFlow[nbNodes];
    SI.HeatFlow heatFlow "Heat flow from Hot to Cold";

    parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.2 "Nominal mass flow rate";
    parameter Modelica.SIunits.Temperature T_inletCold_nominal=5 + 273.15
      "Nominal cold inlet temperature";
    parameter Modelica.SIunits.Temperature T_outletCold_nominal=10 + 273.15
      "Nominal cold outlet temperature";
    parameter Modelica.SIunits.Temperature T_inletHot_nominal=30 + 273.15
      "Nominal hot inlet temperature";
    parameter Modelica.SIunits.Temperature T_outletHot_nominal=15 + 273.15
      "Nominal hot outlet temperature";
    parameter Modelica.SIunits.HeatFlowRate Q_flow_nominal = m_flow_nominal*4200*(T_inletCold_nominal-T_outletCold_nominal)
      "Nominal heat transfer";

  algorithm

    if material == CO.MaterialConstants.Material.Copper then
      k := 400.0 "Values for k are taken from https://www.engineeringtoolbox.com/thermal-conductivity-d_429.html";
      wallDensity := 8940;
      CWall := wallDensity*0.39 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
    elseif material == CO.MaterialConstants.Material.PEX then
      k := 0.50;
      wallDensity := 940;
      CWall := wallDensity*0.55 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
    elseif material == CO.MaterialConstants.Material.Steel then
      k := 16;
      wallDensity := 7850;
      CWall := wallDensity*0.5024 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
    else
      k := 0.0;
      wallDensity := 0;
      CWall := 0.0;
    end if;

    UAnominal := Q_flow_nominal/Buildings.Fluid.HeatExchangers.BaseClasses.lmtd(
      T_inletCold_nominal,
      T_outletCold_nominal,
      T_inletHot_nominal,
      T_outletHot_nominal) "UA_specific = Q / log mean temperature difference, we compute corresponding UA from this formula.";

    for i in 1:nbNodes loop
      nodeVolumeHotFlow[i] := volumeHotFlow/nbNodes "Volume of each node in Liters. The pipe is divided into nbNodes equal sections";
      nodeVolumeColdFlow[i] := volumeColdFlow/nbNodes;
      nodeVolumeWall[i] := volumeWall/nbNodes;
    end for;

  equation

    for i in 1:nbNodes loop
      // energy balances for each node.
      if i == 1 then
        CO.VolSpecificHeatCapWater * nodeVolumeHotFlow[i] * der(nodeHotFlowT[i]) = inletHot.water * CO.VolSpecificHeatCapWater * (inletHot.T - nodeHotFlowT[i]) - UAnominal * (nodeHotFlowT[i] - nodeWallT[i]);
        // Hot Fluid 2 Wall
        if not counterFlow then
          CO.VolSpecificHeatCapWater * nodeVolumeColdFlow[i] * der(nodeColdFlowT[i]) = inletCold.water * CO.VolSpecificHeatCapWater * (inletCold.T - nodeColdFlowT[i]) - UAnominal * (nodeColdFlowT[i] - nodeWallT[i]);
          // Cold Fluid 2 Wall parallel flow
        else
          CO.VolSpecificHeatCapWater * nodeVolumeColdFlow[nbNodes + 1 - i] * der(nodeColdFlowT[nbNodes + 1 - i]) = inletCold.water * CO.VolSpecificHeatCapWater * (inletCold.T - nodeColdFlowT[nbNodes + 1 - i]) - UAnominal * (nodeColdFlowT[nbNodes + 1 - i] - nodeWallT[nbNodes + 1 - i]);
          // Cold Fluid 2 Wall counterflow
        end if;
      elseif i <> 1 then
        CO.VolSpecificHeatCapWater * nodeVolumeHotFlow[i] * der(nodeHotFlowT[i]) = inletHot.water * CO.VolSpecificHeatCapWater * (nodeHotFlowT[i - 1] - nodeHotFlowT[i]) - UAnominal * (nodeHotFlowT[i] - nodeWallT[i]);
        // Hot Fluid 2 Wall
        if not counterFlow then
          CO.VolSpecificHeatCapWater * nodeVolumeColdFlow[i] * der(nodeColdFlowT[i]) = inletCold.water * CO.VolSpecificHeatCapWater * (nodeColdFlowT[i - 1] - nodeColdFlowT[i]) - UAnominal * (nodeColdFlowT[i] - nodeWallT[i]);
          // Cold Fluid 2 Wall parallel flow
        else
          CO.VolSpecificHeatCapWater * nodeVolumeColdFlow[nbNodes + 1 - i] * der(nodeColdFlowT[nbNodes + 1 - i]) = inletCold.water * CO.VolSpecificHeatCapWater * (nodeColdFlowT[nbNodes + 2 - i] - nodeColdFlowT[nbNodes + 1 - i]) - UAnominal * (nodeColdFlowT[nbNodes + 1 - i] - nodeWallT[nbNodes + 1 - i]);
          // Cold Fluid 2 Wall counterflow
        end if;
      end if;
  if not thermalLosses then
        CWall * nodeVolumeWall[i] * der(nodeWallT[i]) = UAnominal * (nodeHotFlowT[i] - nodeWallT[i]) + UAnominal * (nodeColdFlowT[i] - nodeWallT[i]);
        //Wall to Hot and Cold Flow;
      else
        CWall * nodeVolumeWall[i] * der(nodeWallT[i]) = UAnominal * (nodeHotFlowT[i] - nodeWallT[i]) + UAnominal * (nodeColdFlowT[i] - nodeWallT[i]) + UAnominal / 100 * (tEnvironment - nodeWallT[i]);
        //Wall to Hot and Cold Flow, with thermal losses. We consider the losses to the environment being 100 times less than the heat transfer between pipes.;
      end if;

    nodeHeatFlow[i] = - UAnominal * (nodeColdFlowT[i] - nodeWallT[i]);

    end for;

    inletCold.water + outletCold.water = 0 "flow balance";
    inletHot.water + outletHot.water = 0 "flow balance";

    outletHot.T = nodeHotFlowT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    if not counterFlow then
      outletCold.T = nodeColdFlowT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    else
      outletCold.T = nodeColdFlowT[1] "The outlet temperature is equal to the temperature of the last node (node 1 if counterflow)";
    end if;

    heatFlow = sum(nodeHeatFlow);
    sum(- UAnominal/100 * (tEnvironment .- nodeWallT)) + heatOutlet.heat = 0;

    annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics={  Line(points = {{-92.4, 92.4}, {92.4, -92.4}}, color = {0, 0, 0}, thickness = 0.5), Line(points = {{0, -100}, {-5, -5}, {-100, 0}}, color = {0, 0, 255}, thickness = 1.5, smooth = Smooth.Bezier), Line(points = {{0, 100}, {5, 5}, {100, 0}}, color = {208, 52, 43}, thickness = 1.5, smooth = Smooth.Bezier)}));
  end DiscretizedHEX;

  package Examples
    extends Modelica.Icons.ExamplesPackage;

    model HeatExchangersExample
      extends Modelica.Icons.Example;
      ImportExport.Imports.EnergySupplySO energySupplySO1 annotation (
        Placement(visible = true, transformation(origin = {-96, 62}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n=3) annotation (
        Placement(visible = true, transformation(origin = {-98, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Boilers.InstantaneousBoiler instantaneousBoiler1
        (n=2) annotation (
        Placement(visible = true, transformation(origin = {-56, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.HeatExchangers.NotSoSimpleHeatExchanger
        notSoSimpleHeatExchanger1 annotation (
        Placement(visible = true, transformation(origin = {14, 72}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.HeatExchangers.NotSoSimpleHeatExchanger
        notSoSimpleHeatExchanger2 annotation (
        Placement(visible = true, transformation(origin = {16, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Taps.ClassicTap Tap annotation (
        Placement(visible = true, transformation(origin = {2, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Taps.ClassicTap Tap1 annotation (
        Placement(visible = true, transformation(origin = {2, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ImportExport.Exports.WaterSinkMI waterSinkMI1(n=2) annotation (
        Placement(visible = true, transformation(origin = {66, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile TapHyd(fileName=
            "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/TapChildren_tmp.csv")
        annotation (
        Placement(visible = true, transformation(origin = {-12, 122}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile Tap1Hyd(fileName=
            "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/TapHousehold_tmp.csv")
        annotation (
        Placement(visible = true, transformation(origin = {-12, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(Tap1Hyd.y, Tap1.flowInput) annotation (
        Line(points={{-1,34},{2,34},{2,22},{2,21.6},{2,21.6}},       color = {0, 0, 127}, thickness = 0.5));
      connect(TapHyd.y, Tap.flowInput) annotation (
        Line(points={{-1,122},{4,122},{4,108},{2,108},{2,107.6}},         color = {0, 0, 127}, thickness = 0.5));
      connect(notSoSimpleHeatExchanger1.outletHot, waterSinkMI1.inlet[2]) annotation (
        Line(points={{24,72},{40,72},{40,40},{56,40},{56,42},{56,42}},              color = {0, 127, 255}));
      connect(notSoSimpleHeatExchanger2.outletHot, waterSinkMI1.inlet[1]) annotation (
        Line(points={{26,-16},{40,-16},{40,40},{56,40},{56,38},{56,38}},              color = {0, 127, 255}));
      connect(Tap1.outlet, notSoSimpleHeatExchanger2.inletHot) annotation (
        Line(points = {{12, 12}, {16, 12}, {16, -6}, {16, -6}, {16, -6}}, color = {0, 127, 255}));
      connect(notSoSimpleHeatExchanger2.outletCold, Tap1.inletCold) annotation (
        Line(points = {{6, -16}, {-16, -16}, {-16, 10}, {-8, 10}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], notSoSimpleHeatExchanger2.inletCold) annotation (
        Line(points = {{-88, -2}, {-74, -2}, {-74, -52}, {16, -52}, {16, -26}}, color = {0, 127, 255}));
      connect(Tap.outlet, notSoSimpleHeatExchanger1.inletHot) annotation (
        Line(points = {{12, 98}, {14, 98}, {14, 82}, {14, 82}, {14, 82}}, color = {0, 127, 255}));
    connect(notSoSimpleHeatExchanger1.outletCold, Tap.inletCold) annotation (
        Line(points = {{4, 72}, {-18, 72}, {-18, 96}, {-8, 96}}, color = {0, 127, 255}));
    connect(waterSupplyMO1.outlets[3], notSoSimpleHeatExchanger1.inletCold) annotation (
        Line(points={{-88,0.666667},{-74,0.666667},{-74,52},{14,52},{14,62}},color = {0, 127, 255}));
      connect(instantaneousBoiler1.outlet[2], Tap1.inletHot) annotation (
        Line(points={{-46,30},{-32,30},{-32,14},{-6,14},{-6,14},{-8,14},{-8,14}},                color = {0, 127, 255}));
      connect(instantaneousBoiler1.outlet[1], Tap.inletHot) annotation (
        Line(points={{-46,26},{-32,26},{-32,100},{-8,100},{-8,100},{-8,100}},              color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[1], instantaneousBoiler1.inlet) annotation (
        Line(points={{-88,-4.66667},{-74,-4.66667},{-74,28},{-66,28},{-66,28},{
              -66,28}},                                                                   color = {0, 127, 255}));
      connect(energySupplySO1.outlet, instantaneousBoiler1.energy_in) annotation (
        Line(points = {{-86, 62}, {-56, 62}, {-56, 38}, {-56, 38}}, color = {208, 52, 5}));
      annotation (Documentation(info="<html>
<p>This example demonstrates a simple use of a heat exchanger that pre-heats the cold water coming into taps. Note that this is just a hypothetical scenario: tap water use is, generally, too short for heat exchange in real-world.</p>
</html>"));
    end HeatExchangersExample;
  end Examples;

  annotation (Documentation(info="<html>
<p>Heat exchangers, as the name suggest, allow the exchange of cold and hot water coming into an appliance. The heat exchange is in terms of user-specified nominal heat transfer in Watts.  </p>
</html>"));
end HeatExchangers;
