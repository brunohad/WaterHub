within WaterHub.DHWTechnologies;
package Tanks
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  extends Modelica.Icons.Package;

  partial model BaseOpenTank
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n = 1 "number of inlet ports";
    WaterHub.BaseClasses.WaterPorts_in[n] inlet annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -34}, {10, 34}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -34}, {10, 34}}, rotation = 0)));
    BaseClasses.WaterPort_out outlet annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    SI.Volume volume(min = 0.0, start = minVolume);
    SI.AbsoluteTemperature volumeT(min = 273.15, start = 273.15 + 20);
    parameter SI.Volume totalVolume = 400 "max volume in tank";
    parameter SI.Volume maxVolume = 200 "upper vollume limit in tank";
    parameter SI.Volume minVolume = 10 "lower volume limit in tank";
    parameter SI.WaterFlow maxFlow = 2 "max flow through valve";
    Boolean openValve(start = false);
  algorithm
    when {volume < minVolume, volume > maxVolume} then
      openValve := volume > maxVolume;
    end when;
//open valve when volume reaches maxVolume, and close it when it reaches minVolume.
  equation
    der(volume) = sum(inlet.water) + outlet.water;
    outlet.water = if openValve then -maxFlow else 0;
// flow balance
    outlet.T = volumeT;
//outlet temperature is volume temperature
    annotation (
      defaultComponentName = "OpenTank",
      Diagram,
  Icon(graphics = {Line(origin = {0.894909, 2.14147}, points = {{-96, -2.64645}, {-84, 7.35355}, {-70, -6.64645}, {-56, 7.35355}, {-42, -6.64645}, {-28, 7.35355}, {-14, -6.64645}, {0, 7.35355}, {14, -6.64645}, {28, 7.35355}, {42, -6.64645}, {56, 7.35355}, {70, -6.64645}, {84, 7.35355}, {96, -2.64645}, {96, -2.6465}}, color = {3, 154, 255}, thickness = 2, smooth = Smooth.Bezier), Line(origin = {87.5256, -35}, points = {{0, 37}, {0, -37}, {0, -37}}, color = {85, 85, 0}, thickness = 1, arrow = {Arrow.Open, Arrow.Open}), Line(origin = {0.298295, 2.33239}, points = {{-100, 78}, {-100, -78}, {100, -78}, {100, 76}, {100, 76}}, color = {85, 85, 0}, thickness = 4)}));
  end BaseOpenTank;

  model SimpleOpenTank
    extends BaseOpenTank;
  equation
    der(volumeT * volume) = inlet.water * inlet.T + outlet.water * outlet.T;
// ideal heat balance
    annotation (
      Documentation(info = "<html><head></head><body><p>SimpleOpenTank adds onto the hydraulics of BaseOpenTank an ideal heat balance: the heat budget contained in the tank (volumeT * volume) is governed by heat coming in and out of the tank.</p></body></html>"),
      Icon(graphics = {Line(origin = {0.894909, 2.14147}, points = {{-96, -2.64645}, {-84, 7.35355}, {-70, -6.64645}, {-56, 7.35355}, {-42, -6.64645}, {-28, 7.35355}, {-14, -6.64645}, {0, 7.35355}, {14, -6.64645}, {28, 7.35355}, {42, -6.64645}, {56, 7.35355}, {70, -6.64645}, {84, 7.35355}, {96, -2.64645}, {96, -2.6465}}, color = {3, 154, 255}, thickness = 2, smooth = Smooth.Bezier), Line(origin = {87.5256, -35}, points = {{0, 37}, {0, -37}, {0, -37}}, color = {85, 85, 0}, thickness = 1, arrow = {Arrow.Open, Arrow.Open}), Line(origin = {0.298295, 2.33239}, points = {{-100, 78}, {-100, -78}, {100, -78}, {100, 76}, {100, 76}}, color = {85, 85, 0}, thickness = 4)}));
  end SimpleOpenTank;

  model HEX_OpenTank
    extends BaseOpenTank;
    WaterHub.BaseClasses.WaterPort_in HEXport_in annotation (
      Placement(visible = true, transformation(origin = {60, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {60, -74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    WaterHub.BaseClasses.WaterPort_out HEXport_out annotation (
      Placement(visible = true, transformation(origin = {-60, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-60, -76}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    SI.HeatFlow heatFlow;
    parameter SI.AbsoluteTemperature tEnvironment = 293.15;
    SI.OverallConductance UA(start=0.0);
    parameter SI.Height tankHeight = 1 "Height of tank";
    final parameter SI.Length tankDiameter = sqrt((totalVolume/1000)/(Constants.pi * tankHeight));
    SI.Area wetArea;
    WaterHub.BaseClasses.HeatPort_out heatPort_out annotation(
      Placement(visible = true, transformation(origin = {100, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    // UA Computation
    wetArea = 2*(volume/1000)*((tankDiameter+tankHeight)/(tankDiameter*tankHeight));
    1/UA = (1/(3000*wetArea))+(0.01/(0.2*wetArea))+(1/(50*wetArea)) "computation of overall heat transfer coeff with hypothesis: 1cm plastic tank (about 0.2 W/m K thermal conductivity, 3000 W/m2 K heat transfer coeff for water and 50 W/m2 K for air - those are arbitrary middle-of-the-range values. Note that the wet area is dependent on the time-varying volume variable.";  
  
    HEXport_out.T = volumeT;
// setpoint temperature of ideal HEX is temperature of tank volume;
    HEXport_in.water + HEXport_out.water = 0;

    HEXport_in.water * HEXport_in.T + HEXport_out.water * HEXport_out.T + heatFlow / CO.VolSpecificHeatCapWater = 0;
//prescribed heat flow between tank water and water flowing through HEX
    der(volumeT * volume) = inlet.water * inlet.T + outlet.water * outlet.T - (heatFlow + UA * (volumeT - tEnvironment)) / CO.VolSpecificHeatCapWater;
    
    
    heatPort_out.heat + UA * (volumeT - tEnvironment) / CO.VolSpecificHeatCapWater = 0;
  
  annotation (Documentation(info = "<html>
<p>In addition to the features of Simple Open Tank, the Heat Exchanger Open Tank allows heat exchange.</p>
</html>"),
      Icon(graphics = {Line(origin = {0.596614, 2.14147}, points = {{-96, -2.64645}, {-84, 7.35355}, {-70, -6.64645}, {-56, 7.35355}, {-42, -6.64645}, {-28, 7.35355}, {-14, -6.64645}, {0, 7.35355}, {14, -6.64645}, {28, 7.35355}, {42, -6.64645}, {56, 7.35355}, {70, -6.64645}, {84, 7.35355}, {96, -2.64645}, {96, -2.6465}}, color = {3, 154, 255}, thickness = 2, smooth = Smooth.Bezier), Line(origin = {86.55, -34.58}, points = {{0, 37}, {0, -37}, {0, -37}}, color = {85, 85, 0}, thickness = 0.5, arrow = {Arrow.Open, Arrow.Open}), Line(origin = {0, 2.33239}, points = {{-100, 78}, {-100, -78}, {100, -78}, {100, 76}, {100, 76}}, color = {85, 85, 0}, thickness = 4), Line(origin = {-1, -46}, points = {{61, -28}, {61, 30}, {45, 30}, {45, -4}, {51, -4}, {51, 30}, {23, 30}, {23, -4}, {29, -4}, {29, 30}, {-3, 30}, {-3, -4}, {3, -4}, {3, 30}, {-29, 30}, {-29, -4}, {-23, -4}, {-23, 30}, {-55, 30}, {-55, -4}, {-49, -4}, {-49, 30}, {-61, 30}, {-61, -30}, {-61, -30}}, color = {28, 108, 200}, thickness = 1.5, smooth = Smooth.Bezier), Line(origin = {-30, -46}, points = {{-32, -30}, {-32, 30}, {-20, 30}, {-20, -4}, {-26, -4}, {-26, 30}, {6, 30}, {6, -4}, {0, -4}, {0, 30}, {32, 30}, {32, -4}, {32, 16}}, color = {170, 0, 0}, thickness = 1.5, smooth = Smooth.Bezier)}));
  end HEX_OpenTank;

  package Examples
    extends Modelica.Icons.Example;

    model SimpleOpenTank_Example
      extends Modelica.Icons.Example;
      WaterHub.Appliances.Showers.ClassicShower Shower annotation (
        Placement(visible = true, transformation(origin = {30, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 2) annotation (
        Placement(visible = true, transformation(origin = {-114, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.EnergySupplySO energySupplySO1 annotation (
        Placement(visible = true, transformation(origin = {-112, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile ShowerHyd(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/Shower_tmp.csv") annotation (
        Placement(visible = true, transformation(origin = {16, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
        pipe(nbNodes=5) annotation (Placement(visible=true, transformation(
            origin={-28,6},
            extent={{-10,-10},{10,10}},
            rotation=0)));
      WaterHub.DHWTechnologies.Boilers.TankBoiler tankBoiler1(n = 1)  annotation (
        Placement(visible = true, transformation(origin = {-76, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.HeatSinkMI heatSinkMI1(n = 2) annotation (
        Placement(visible = true, transformation(origin = {-14, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1(n = 1)  annotation (
        Placement(visible = true, transformation(origin = {114, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Tanks.SimpleOpenTank OpenTank(n = 1)  annotation (
        Placement(visible = true, transformation(origin = {80, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(pipe.heatOutlet, heatSinkMI1.inlet[2]) annotation (
        Line(points={{-28,2.5},{-28,2.5},{-28,-64},{-24,-64},{-24,-62}},        color = {208, 52, 5}));
      connect(pipe.outlet, Shower.inletHot) annotation (
        Line(points = {{-18, 6}, {12, 6}, {12, 0}, {20, 0}, {20, 0}, {20, 0}}, color = {0, 127, 255}));
      connect(tankBoiler1.outlet[1], pipe.inlet) annotation (
        Line(points = {{-66, 6}, {-38, 6}, {-38, 6}, {-38, 6}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[1], tankBoiler1.inlet) annotation (
        Line(points={{-104,-26},{-94,-26},{-94,6},{-86,6}},          color = {0, 127, 255}));
      connect(tankBoiler1.heatOutlet, heatSinkMI1.inlet[1]) annotation (
        Line(points={{-76,-4},{-76,-66},{-24,-66},{-24,-66}},          color = {208, 52, 5}));
      connect(energySupplySO1.outlet, tankBoiler1.energy_in) annotation (
        Line(points = {{-102, 34}, {-76, 34}, {-76, 16}}, color = {208, 52, 5}));
      connect(OpenTank.outlet, waterSinkMI1.inlet[1]) annotation (
        Line(points = {{90, -2}, {104, -2}}, color = {0, 127, 255}));
      connect(Shower.outlet, OpenTank.inlet[1]) annotation (
        Line(points = {{40, -2}, {70, -2}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], Shower.inletCold) annotation (
        Line(points={{-104,-22},{-84,-22},{-84,-4},{20,-4}},          color = {0, 127, 255}));
      connect(ShowerHyd.y, Shower.flowInput) annotation (
        Line(points={{27,26},{30,26},{30,7.6}},      color = {0, 0, 127}, thickness = 0.5));
      annotation (
        __OpenModelica_simulationFlags(lv = "LOG_STATS", outputFormat = "mat", s = "dassl"),
          Documentation(info="<html>
<p>This is an example of a system demonstrating the use of a simple open tank. Used warm water from the shower drain flows into the tank at a maximum flow rate of 2 L/s. Water is released from the tank once it exceeds the maximum volume of 200L. </p>
</html>"));
    end SimpleOpenTank_Example;

    model HEX_OpenTank_Example
      extends Modelica.Icons.Example;
      WaterHub.Appliances.Showers.ClassicShower Shower annotation (
        Placement(visible = true, transformation(origin = {30, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 2) annotation (
        Placement(visible = true, transformation(origin = {-112, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.EnergySupplySO energySupplySO1 annotation (
        Placement(visible = true, transformation(origin = {-112, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile ShowerHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Shower_tmp.txt")) annotation (
        Placement(visible = true, transformation(origin = {-8, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
        pipe annotation (Placement(visible=true, transformation(
            origin={-24,8},
            extent={{-10,-10},{10,10}},
            rotation=0)));
      WaterHub.DHWTechnologies.Boilers.TankBoiler tankBoiler1 annotation (
        Placement(visible = true, transformation(origin = {-58, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.HeatSinkMI heatSinkMI1(n = 3) annotation (
        Placement(visible = true, transformation(origin = {-58, -116}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1 annotation (
        Placement(visible = true, transformation(origin = {144, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Tanks.HEX_OpenTank hEX_OpenTank1(maxVolume = 35, minVolume = 5) annotation (
        Placement(visible = true, transformation(origin = {70, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
  connect(hEX_OpenTank1.outlet, waterSinkMI1.inlet[1]) annotation(
        Line(points = {{80, -38}, {96, -38}, {96, 0}, {134, 0}}, color = {0, 127, 255}));
  connect(waterSupplyMO1.outlets[1], hEX_OpenTank1.HEXport_in) annotation(
        Line(points = {{-102, -26}, {-88, -26}, {-88, -74}, {76, -74}, {76, -48}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], Shower.inletCold) annotation (
        Line(points={{-102,-22},{-40,-22},{-40,0},{20,0}},          color = {0, 127, 255}, thickness = 0.5));
  connect(Shower.outlet, hEX_OpenTank1.inlet[1]) annotation(
        Line(points = {{40, 2}, {48, 2}, {48, -38}, {60, -38}}, color = {0, 127, 255}));
  connect(hEX_OpenTank1.HEXport_out, tankBoiler1.inlet) annotation(
        Line(points = {{64, -48}, {64, -66}, {-82, -66}, {-82, 8}, {-68, 8}}, color = {0, 127, 255}));
      connect(tankBoiler1.heatOutlet, heatSinkMI1.inlet[1]) annotation (
        Line(points={{-58,-2},{-58,-54},{-58,-106},{-60,-106}},
                                                color = {208, 52, 5}));
      connect(pipe.heatOutlet, heatSinkMI1.inlet[2]) annotation (
        Line(points={{-24,4.5},{-24,-48},{-56,-48},{-56,-106}},        color = {208, 52, 5}));
      connect(energySupplySO1.outlet, tankBoiler1.energy_in) annotation (
        Line(points = {{-102, 34}, {-58, 34}, {-58, 18}}, color = {208, 52, 5}));
      connect(tankBoiler1.outlet[1], pipe.inlet) annotation (
        Line(points = {{-48, 8}, {-34, 8}}, color = {0, 127, 255}, thickness = 0.5));
      connect(ShowerHyd.y, Shower.flowInput) annotation (
        Line(points={{3,38},{30,38},{30,11.6}},      color = {0, 0, 127}, thickness = 0.5));
      connect(pipe.outlet, Shower.inletHot) annotation (
        Line(points = {{-14, 8}, {6, 8}, {6, 4}, {20, 4}, {20, 4}}, color = {0, 127, 255}));
  connect(hEX_OpenTank1.heatPort_out, heatSinkMI1.inlet[3]) annotation(
        Line(points = {{80, -42}, {94, -42}, {94, -98}, {-58, -98}, {-58, -106}}, color = {208, 52, 5}));
      annotation (
        __OpenModelica_simulationFlags(lv = "LOG_STATS", outputFormat = "mat", s = "dassl"),
          Documentation(info="<html>
<p>In this example, warm water from shower drain is passed on to a heat exchaning open tank that pre-heats the cold water flowing into the tank boiler.</p>
</html>"));
    end HEX_OpenTank_Example;
  end Examples;
end Tanks;
