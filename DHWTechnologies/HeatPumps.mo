within WaterHub.DHWTechnologies;
package HeatPumps
  import SI=WaterHub.SIUnits;
  import CO=WaterHub.Constants;
  extends Modelica.Icons.Package;

  model WaterSourceHeatPump
    extends Modelica.Icons.Package;
    Buildings.Fluid.HeatPumps.Carnot_TCon heaPum annotation (
      Placement(visible = true, transformation(origin = {-1, -1}, extent = {{-25, -25}, {25, 25}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput T_set annotation (
      Placement(visible = true, transformation(origin = {0, 106}, extent = {{-14, -14}, {14, 14}}, rotation = -90), iconTransformation(origin = {0, 106}, extent = {{-14, -14}, {14, 14}}, rotation = -90)));
  WaterHub.BaseClasses.WaterPort_in port_in_a annotation (
      Placement(visible = true, transformation(origin = {-100, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.BaseClasses.WaterPort_out port_out_a annotation (
      Placement(visible = true, transformation(origin = {100, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.BaseClasses.WaterPort_in port_in_b annotation (
      Placement(visible = true, transformation(origin = {100, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.BaseClasses.WaterPort_out port_out_b annotation (
      Placement(visible = true, transformation(origin = {-100, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_WaterSISO1 annotation (
      Placement(visible = true, transformation(origin = {49, -15}, extent = {{7, -7}, {-7, 7}}, rotation = 0)));
  WaterHub.Blocks.Interfaces.WaterHub2MSL_WaterSISO waterHub2MSL_WaterSISO2 annotation (
      Placement(visible = true, transformation(origin = {-73, 15}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
  WaterHub.Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_WaterSISO1 annotation (
      Placement(visible = true, transformation(origin = {55, 15}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
  WaterHub.Blocks.Interfaces.MSL2WaterHub_WaterSISO mSL2WaterHub_WaterSISO2 annotation (
      Placement(visible = true, transformation(origin = {-73, -17}, extent = {{7, -7}, {-7, 7}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput EvaPower annotation (
      Placement(visible = true, transformation(origin = {32, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {32, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.HeatPort_in ComEnergy_in annotation (
      Placement(visible = true, transformation(origin = {-60, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-60, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(heaPum.QEva_flow, EvaPower) annotation (
      Line(points={{26.5,-23.5},{26.5,-23.5},{26.5,-40},{32,-40}},color = {0, 0, 127}));
    connect(mSL2WaterHub_WaterSISO2.port_in, heaPum.port_b2) annotation (
      Line(points={{-66,-17},{-26,-17},{-26,-16},{-26,-16}},          color = {0, 127, 255}));
    connect(port_out_b, mSL2WaterHub_WaterSISO2.port_out) annotation (
      Line(points={{-100,-60},{-86,-60},{-86,-16},{-80,-16},{-80,-17}},            color = {0, 127, 255}));
    connect(mSL2WaterHub_WaterSISO1.port_in, heaPum.port_b1) annotation (
      Line(points={{48,15},{24,15},{24,14},{24,14}},          color = {0, 127, 255}));
    connect(port_out_a, mSL2WaterHub_WaterSISO1.port_out) annotation (
      Line(points={{100,60},{80,60},{80,16},{62,16},{62,15}},            color = {0, 127, 255}));
    connect(waterHub2MSL_WaterSISO2.port_out, heaPum.port_a1) annotation (
      Line(points={{-66,15},{-38,15},{-38,14},{-26,14},{-26,14}},            color = {0, 127, 255}));
    connect(port_in_a, waterHub2MSL_WaterSISO2.port_in) annotation (
      Line(points={{-100,60},{-88,60},{-88,16},{-80,16},{-80,15}},            color = {0, 127, 255}));
    connect(waterHub2MSL_WaterSISO1.port_out, heaPum.port_a2) annotation (
      Line(points = {{42, -15}, {24, -15}, {24, -16}}, color = {0, 127, 255}));
    connect(port_in_b, waterHub2MSL_WaterSISO1.port_in) annotation (
      Line(points = {{100, -60}, {78, -60}, {78, -15}, {56, -15}}, color = {0, 127, 255}));
    connect(T_set, heaPum.TSet) annotation (
      Line(points={{0,106},{0,106},{0,58},{-60,58},{-60,22},{-31,22},{-31,21.5}},              color = {0, 0, 127}));

    ComEnergy_in.heat = -heaPum.P
    annotation (
    Icon(coordinateSystem(preserveAspectRatio=false,extent={{-100,-100},
              {100,100}}),       graphics={
          Rectangle(
            extent={{-70,80},{70,-80}},
            lineColor={0,0,255},
            pattern=LinePattern.None,
            fillColor={95,95,95},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-56,68},{58,50}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-56,-52},{58,-70}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-103,64},{98,54}},
            lineColor={0,0,255},
            pattern=LinePattern.None,
            fillColor={0,0,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-2,54},{98,64}},
            lineColor={0,0,255},
            pattern=LinePattern.None,
            fillColor={255,0,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-101,-56},{100,-66}},
            lineColor={0,0,255},
            pattern=LinePattern.None,
            fillColor={0,0,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-100,-66},{0,-56}},
            lineColor={0,0,127},
            pattern=LinePattern.None,
            fillColor={0,0,127},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-42,0},{-52,-12},{-32,-12},{-42,0}},
            lineColor={0,0,0},
            smooth=Smooth.None,
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-42,0},{-52,10},{-32,10},{-42,0}},
            lineColor={0,0,0},
            smooth=Smooth.None,
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-44,50},{-40,10}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-44,-12},{-40,-52}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{38,50},{42,-52}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{18,22},{62,-20}},
            lineColor={0,0,0},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{40,22},{22,-10},{58,-10},{40,22}},
            lineColor={0,0,0},
            smooth=Smooth.None,
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid)}));


    annotation (Documentation(info="<html>
<p>A heat pump is device that transfers heat from a cold source to a hot one.</p>
</html>"));
  end WaterSourceHeatPump;

end HeatPumps;
