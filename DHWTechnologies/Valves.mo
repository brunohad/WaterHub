within WaterHub.DHWTechnologies;
package Valves
  extends Modelica.Icons.Package;

  model MixingValve
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of inlet ports";
    WaterHub.BaseClasses.WaterPorts_in[n] ports_in annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -40}, {10, 40}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -40}, {10, 40}}, rotation = 0)));
    BaseClasses.WaterPort_out port_out annotation (
      Placement(visible = true, transformation(origin = {100, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

  equation
      sum(ports_in.water) + port_out.water = 0;
      ports_in.water*ports_in.T + port_out.water*port_out.T = 0;

  end MixingValve;

end Valves;
