within WaterHub.DHWTechnologies;
package Boilers
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  extends Modelica.Icons.Package;

  partial model BaseBoiler
    extends Modelica.Icons.BasesPackage;
    parameter SI.AbsoluteTemperature setPointT = 333 "Set Point Temperature";
    parameter Integer n=1 "Number of outlet ports";

    //inlet ports
    WaterHub.BaseClasses.HeatPort_in energy_in(heat(min=-1e-5, start=0))
    annotation (Placement(transformation(extent={{-10,90},{10,110}})));
    WaterHub.BaseClasses.WaterPort_in inlet(water(min=-10e-5))
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
    //outlet ports
    WaterHub.BaseClasses.WaterPorts_out[n] outlet(each water(max=1e-5))
    annotation (Placement(transformation(extent={{90,-40},{110,40}})));
    annotation (defaultComponentName="Boiler");
  end BaseBoiler;

  model InstantaneousBoiler
    extends BaseBoiler;
    extends WaterHub.Icons.ModelIcon;

  equation
    for i in 1:n loop
      outlet[i].T - setPointT = 0;
    end for;
    outlet.T*outlet.water + inlet.T*inlet.water + energy_in.heat/CO.VolSpecificHeatCapWater = 0 "scalar product of outlet ports temp and volume; energy balance";
    sum(outlet.water) + inlet.water = 0 "Sum of all outlet ports and inlet; mass balance";

  end InstantaneousBoiler;

  model InstantaneousBoilerWithSchedule
    extends BaseBoiler;
    extends WaterHub.Icons.ModelIcon;
    Modelica.Blocks.Interfaces.RealInput flowInput "flowInput[1] is setpoint temperature"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}}, rotation=-45)));

  equation
    for i in 1:n loop
      outlet[i].T - flowInput = 0;
    end for;
    outlet.T*outlet.water + inlet.T*inlet.water + energy_in.heat/CO.VolSpecificHeatCapWater = 0 "scalar product of outlet ports temp and volume; energy balance";
    sum(outlet.water) + inlet.water = 0 "Sum of all outlet ports and inlet; mass balance";

  end InstantaneousBoilerWithSchedule;

//  model TankBoiler "NOTE: THIS MODEL WILL BE DEPRECATED SOON. USE TankBoilerWithLosses INSTEAD. Water tank boiler. Volume is heated up to the set point temperature (Default = 333 K), when the temperature goes below the 'lower set point' (Default = 323 K )."
//    extends BaseBoiler;
//    //heat outlet
//    extends WaterHub.Icons.ModelIcon;
//    WaterHub.BaseClasses.HeatPort_out heatOutlet "Losses - connects to heat sink" annotation(
//      Placement(transformation(extent = {{-10, -110}, {10, -90}})));

//    parameter SI.AbsoluteTemperature lowSetPointT = 330 "Lower set point temperature";
//    parameter SI.Volume volume = 200 "Water tank volume";
//    SI.AbsoluteTemperature volumeT(start=setPointT) "Temperature of water volume";
//    parameter SI.Power maxPower = 1000 "Maximum heater power";
//    parameter SI.Power loss = 1 "Lost energy";

////  initial equation
////    energy_in.heat = 0 "necessary to avoid errors when initializing the boiler temperature inbetween set points";
//  equation
//    for i in 1:n loop
//      outlet[i].T - volumeT = 0;
//    end for;

//    sum(outlet.water) + inlet.water = 0 "Sum of all outlet ports and inlet; mass balance";
//    outlet.T*outlet.water + inlet.T*inlet.water - der(volumeT*volume) + energy_in.heat/CO.VolSpecificHeatCapWater - loss/CO.VolSpecificHeatCapWater = 0 "Energy balance";

//    // Operation control
//    if volumeT < lowSetPointT then
//	 energy_in.heat = maxPower;
//    elseif volumeT > setPointT then
//	 energy_in.heat = 0;
//    else
//	 energy_in.heat = pre(energy_in.heat);
//    end if;
//    loss + heatOutlet.heat = 0;

//end TankBoiler;

  model TankBoiler "Water tank boiler. Volume is heated up to the set point temperature (Default = 333 K), when the temperature goes below the 'lower set point' (Default = 323 K )."
    extends BaseBoiler;
    //heat outlet
    extends WaterHub.Icons.ModelIcon;
    WaterHub.BaseClasses.HeatPort_out heatOutlet "Losses - connects to heat sink" annotation (
      Placement(transformation(extent = {{-10, -110}, {10, -90}})));
    parameter SI.AbsoluteTemperature lowSetPointT = 330 "Lower set point temperature";
    parameter SI.Volume volume = 200 "Water tank volume";
    SI.AbsoluteTemperature volumeT(start = setPointT) "Temperature of water volume";
    parameter SI.Power maxPower = 1000 "Maximum heater power";
    parameter SI.Power loss = 1 "Lost energy";
    parameter Real cop = 0.9 "Coefficience of Performance";
    Boolean heatOn(start=false);
  /* initial equation
    der(energy_in.heat) = 0 "necessary to avoid errors when initializing the boiler temperature inbetween set points"; */

  algorithm
    // Operation control
    when {volumeT < lowSetPointT, volumeT > setPointT} then
  heatOn := volumeT < setPointT;
    end when;

  equation
    for i in 1:n loop
      outlet[i].T - volumeT = 0;
    end for;
    sum(outlet.water) + inlet.water = 0 "Sum of all outlet ports and inlet; mass balance";
    outlet.T * outlet.water + inlet.T * inlet.water - der(volumeT * volume) + energy_in.heat * cop / CO.VolSpecificHeatCapWater - loss / CO.VolSpecificHeatCapWater = 0 "Energy balance";

    energy_in.heat = if heatOn then maxPower else 0;

    loss + heatOutlet.heat = 0;

    annotation (Documentation(info="<html>
<p>Water tank boiler. Volume is heated up to the set point temperature (Default = 333 K), when the temperature goes below the &apos;lower set point&apos; (Default = 323 K ).</p>
</html>"));
  end TankBoiler;

  model TankBoilerWithSchedule "Water tank boiler. Volume is heated up to the set point temperature (Default = 333 K), when the temperature goes below the 'lower set point' (Default = 323 K )."
    extends BaseBoiler;
    //heat outlet
    extends WaterHub.Icons.ModelIcon;
    Modelica.Blocks.Interfaces.RealInput[2] flowInput "flowInput[1] is lower setPointT, flowInput[2] is upper SetpointT"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}}, rotation=-45)));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "Losses - connects to heat sink" annotation (
      Placement(transformation(extent = {{-10, -110}, {10, -90}})));
    parameter SI.AbsoluteTemperature lowSetPointT = 330 "Lower set point temperature";
    parameter SI.Volume volume = 200 "Water tank volume";
    SI.AbsoluteTemperature volumeT(start = setPointT) "Temperature of water volume";
    parameter SI.Power maxPower = 1000 "Maximum heater power";
    parameter SI.Power loss = 1 "Lost energy";
    parameter Real cop = 0.9 "Coefficience of Performance";
    Boolean heatOn(start=false);
  /* initial equation
    der(energy_in.heat) = 0 "necessary to avoid errors when initializing the boiler temperature inbetween set points"; */

  algorithm
    // Operation control
    when {volumeT < flowInput[1], volumeT > flowInput[2]} then
      heatOn := volumeT < flowInput[2];
    end when;

  equation
    for i in 1:n loop
      outlet[i].T - volumeT = 0;
    end for;
    sum(outlet.water) + inlet.water = 0 "Sum of all outlet ports and inlet; mass balance";
    outlet.T * outlet.water + inlet.T * inlet.water - der(volumeT * volume) + energy_in.heat * cop / CO.VolSpecificHeatCapWater - loss / CO.VolSpecificHeatCapWater = 0 "Energy balance";

    energy_in.heat = if heatOn then maxPower else 0;

    loss + heatOutlet.heat = 0;

    annotation (Documentation(info="<html>
<p>Water tank boiler. Volume is heated up to the set point temperature (Default = 333 K), when the temperature goes below the &apos;lower set point&apos; (Default = 323 K ).</p>
</html>"));
  end TankBoilerWithSchedule;

  model TankBoilerWithCirculationSystem "Water tank boiler with circulation system"
    extends WaterHub.Icons.ModelIcon;
    parameter Integer n=1 "Number of ports";
    Modelica.Blocks.Interfaces.RealInput[2] flowInput "flowInput[1] is lower setPointT, flowInput[2] is upper SetpointT"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}}, rotation=-45)));
    WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSIMO
      pipe(
      material=WaterHub.Constants.MaterialConstants.Material.PEX,
      n=2,
      nbNodes=15,
      pipeDiameterInside=0.05,
      pipeLength=50,
      pipeThickness=0.01) annotation (Placement(visible=true, transformation(
          origin={32,0},
          extent={{-10,-10},{10,10}},
          rotation=0)));
  BaseClasses.HeatPort_in port_in annotation (
      Placement(visible = true, transformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.HeatPort_out port_out annotation (
      Placement(visible = true, transformation(origin = {-2, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-2, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.WaterPort_in port_in1 annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Valves.MixingValve mixingValve1(n=2) annotation (
        Placement(visible=true, transformation(
          origin={-36,0},
          extent={{-10,-10},{10,10}},
          rotation=0)));
  WaterHub.Appliances.Toilet Pump annotation (
      Placement(visible = true, transformation(origin = {-34, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  WaterHub.Blocks.Sources.hydrographFromFile PumpSchedule(columns = {2, 3}, fileName = "/home/hadengbr/Polybox/EAWAG/12_Projects/08_HammesKunz_Legionella/FirstTests/PumpSchedule.csv")  annotation (
      Placement(visible = true, transformation(origin = {-58, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.BaseClasses.HeatPort_out port_out1 annotation (
      Placement(visible = true, transformation(origin = {32, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {32, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BaseClasses.WaterPort_out port_out2 annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Boilers.TankBoilerWithSchedule tankBoiler(
      cop=1,
      loss=100,
      maxPower=10000,
      volume=800) annotation (Placement(visible=true, transformation(
          origin={-2,0},
          extent={{-10,-10},{10,10}},
          rotation=0)));
  equation
    connect(flowInput, tankBoiler.flowInput) annotation (
      Line(points={{2.84217e-14,127.279},{-12,127.279},{-12,12.7279},{-2,
            12.7279}},                                            color = {0, 0, 127}, thickness = 0.5));
    connect(port_in, tankBoiler.energy_in) annotation (
      Line(points = {{0, 100}, {-2, 100}, {-2, 10}, {-2, 10}}));
    connect(mixingValve1.port_out, tankBoiler.inlet) annotation (
      Line(points={{-26,0.4},{-12,0.4},{-12,0},{-12,0}},      color = {0, 127, 255}));
    connect(tankBoiler.heatOutlet, port_out) annotation (
      Line(points = {{-2, -10}, {-2, -10}, {-2, -100}, {-2, -100}}, color = {208, 52, 5}));
    connect(tankBoiler.outlet[1], pipe.inlet) annotation (
      Line(points = {{8, 0}, {22, 0}, {22, 0}, {22, 0}}, color = {0, 127, 255}, thickness = 0.5));
    connect(pipe.outlet[1], port_out2) annotation (
      Line(points={{42,-1.5},{98,-1.5},{98,0},{100,0}},    color = {0, 127, 255}, thickness = 0.5));
    connect(pipe.heatOutlet, port_out1) annotation (
      Line(points={{32,-3.5},{32,-3.5},{32,-100},{32,-100}},      color = {208, 52, 5}));
    connect(PumpSchedule.y, Pump.flowInput) annotation (
      Line(points={{-47,-60},{-33,-60},{-33,-45.6},{-34,-45.6}},      color = {0, 0, 127}, thickness = 0.5));
    connect(Pump.outlet, mixingValve1.ports_in[2]) annotation (
      Line(points={{-44,-36},{-47,-36},{-47,-36},{-50,-36},{-50,-2},{-46,-2},{
            -46,-1},{-46,2},{-46,2}},                                                                                       color = {0, 127, 255}));
    connect(pipe.outlet[2], Pump.inlet) annotation (
      Line(points={{42,1.5},{56,1.5},{56,-36},{-24,-36}},      color = {0, 127, 255}, thickness = 0.5));
    connect(port_in1, mixingValve1.ports_in[1]) annotation (
      Line(points={{-100,0},{-74,0},{-74,-2},{-46,-2}}));
    annotation (Documentation(info="<html>
<p>Water tank boiler with circulation system</p>
</html>"));
  end TankBoilerWithCirculationSystem;

  annotation (Documentation(info="<html>
<p>Water tank boiler with cold water input that boils the water to a set temperature using the supplied energy. </p>
</html>"));
end Boilers;
