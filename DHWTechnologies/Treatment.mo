﻿within WaterHub.DHWTechnologies;
package Treatment
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  extends Modelica.Icons.Package;

  model GWTreatment_MBR "A model simulation GW treatment with MBR: two tanks in a row."
  extends WaterHub.Icons.ModelIcon;
  parameter Integer n = 1 "Number of inlet ports";
  parameter SI.WaterFlow maxFlow1 = 0.5;
  parameter SI.WaterFlow maxFlow2 = 0.5;
  parameter SI.Volume maxVolume1 = 200;
  parameter SI.Volume maxVolume2 = 200;
  parameter SI.Volume minVolume1 = 10;
  parameter SI.Volume minVolume2 = 10;
  WaterHub.BaseClasses.WaterPorts_in[n] ports_in annotation (
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -40}, {10, 40}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -40}, {10, 40}}, rotation = 0)));
  WaterHub.BaseClasses.WaterPort_out port_out annotation (
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Tanks.SimpleOpenTank OpenTank1(n=n, maxFlow = maxFlow1, maxVolume = maxVolume1, minVolume = minVolume1)  annotation (
      Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Tanks.SimpleOpenTank OpenTank2(maxFlow = maxFlow2, maxVolume = maxVolume2, minVolume=minVolume2)  annotation (
      Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
  connect(ports_in, OpenTank1.inlet) annotation (
      Line(points = {{-100, 0}, {-40, 0}}, color = {0, 127, 255}, thickness = 0.5));
  connect(OpenTank1.outlet, OpenTank2.inlet[1]) annotation (
      Line(points = {{-20, 0}, {20, 0}}, color = {0, 127, 255}));
    connect(OpenTank2.outlet, port_out) annotation (
      Line(points = {{40, 0}, {100, 0}, {100, 0}, {100, 0}}, color = {0, 127, 255}));
  annotation (defaultComponentName="MBR_GW_Treatment");
  end GWTreatment_MBR;

  package Examples
    extends Modelica.Icons.Example;

    model GWTreatment_MBR
      extends Modelica.Icons.Example;
      WaterHub.Appliances.Showers.ClassicShower Shower annotation (
        Placement(visible = true, transformation(origin = {30, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 2)  annotation (
        Placement(visible = true, transformation(origin = {-114, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.EnergySupplySO energySupplySO1 annotation (
        Placement(visible = true, transformation(origin = {-112, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile ShowerHyd(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/TapAdults_tmp.csv")  annotation (
        Placement(visible = true, transformation(origin = {-8, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
        pipe annotation (Placement(visible=true, transformation(
            origin={-24,8},
            extent={{-10,-10},{10,10}},
            rotation=0)));
      WaterHub.DHWTechnologies.Boilers.TankBoiler tankBoiler1 annotation (Placement(
            visible=true, transformation(
            origin={-72,8},
            extent={{-10,-10},{10,10}},
            rotation=0)));
      WaterHub.ImportExport.Exports.HeatSinkMI heatSinkMI1(n = 2)  annotation (
        Placement(visible = true, transformation(origin = {-30, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1 annotation (
        Placement(visible = true, transformation(origin = {144, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.DHWTechnologies.Treatment.GWTreatment_MBR MBR_GW_Treatment
        annotation (
        Placement(visible = true, transformation(origin = {84, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(energySupplySO1.outlet, tankBoiler1.energy_in) annotation (
        Line(points = {{-102, 34}, {-72, 34}, {-72, 18}}, color = {208, 52, 5}));
      connect(waterSupplyMO1.outlets[1], tankBoiler1.inlet) annotation (
        Line(points={{-104,-26},{-102,-26},{-102,8},{-82,8}},          color = {0, 127, 255}, thickness = 0.5));
      connect(tankBoiler1.outlet[1], pipe.inlet) annotation (
        Line(points = {{-62, 8}, {-34, 8}}, color = {0, 127, 255}, thickness = 0.5));
      connect(tankBoiler1.heatOutlet, heatSinkMI1.inlet[1]) annotation (
        Line(points={{-72,-2},{-72,-68},{-40,-68},{-40,-68}},          color = {208, 52, 5}));
      connect(MBR_GW_Treatment.port_out, waterSinkMI1.inlet[1]) annotation (
        Line(points = {{94, 2}, {134, 2}, {134, 2}, {134, 2}}, color = {0, 127, 255}));
      connect(Shower.outlet, MBR_GW_Treatment.ports_in[1]) annotation (
        Line(points = {{40, 2}, {74, 2}, {74, 2}, {74, 2}}, color = {0, 127, 255}));
      connect(ShowerHyd.y, Shower.flowInput) annotation (
        Line(points={{3,38},{30,38},{30,11.6}},      color = {0, 0, 127}, thickness = 0.5));
      connect(pipe.heatOutlet, heatSinkMI1.inlet[2]) annotation (
        Line(points={{-24,4.5},{-24,4.5},{-24,-48},{-54,-48},{-54,-66},{-40,-66},
              {-40,-64},{-40,-64}},                                                                                 color = {208, 52, 5}));
      connect(pipe.outlet, Shower.inletHot) annotation (
        Line(points = {{-14, 8}, {6, 8}, {6, 4}, {20, 4}, {20, 4}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], Shower.inletCold) annotation (
        Line(points={{-104,-22},{-40,-22},{-40,0},{20,0}},          color = {0, 127, 255}, thickness = 0.5));
      annotation (
        __OpenModelica_simulationFlags(lv = "LOG_STATS", outputFormat = "mat", s = "dassl"));
    end GWTreatment_MBR;
  end Examples;
end Treatment;
