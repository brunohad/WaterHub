within WaterHub;
package DHWTechnologies "groups building blocks of a DHW system: e.g., boilers or reservoirs. The package also includes resource-recovering technologies, for instance drain water heat exchangers, wastewater-connected heat pumps or decentralized greywater treatment units."
  extends Modelica.Icons.Package;
end DHWTechnologies;
