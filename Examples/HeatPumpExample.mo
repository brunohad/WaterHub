within WaterHub.Examples;
model HeatPumpExample
  extends Modelica.Icons.Example;
  WaterHub.ImportExport.Imports.WaterSupplyMO Water annotation (
    Placement(visible = true, transformation(origin={-118,-32},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Imports.EnergySupplyMO energy annotation (
    Placement(visible = true, transformation(origin = {-120, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Exports.WaterSinkMI Wastewater(n=2)
                                                       annotation (
    Placement(visible = true, transformation(origin={110,8},     extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Imports.WaterSupplyMO Water2(n=2)
                                                     annotation (
    Placement(visible = true, transformation(origin={110,-28},    extent = {{10, -10}, {-10, 10}}, rotation = 0)));

  DHWTechnologies.HeatPumps.WaterSourceHeatPump waterSourceHeatPump
    annotation (Placement(transformation(extent={{-34,-8},{-14,12}})));
  Modelica.Blocks.Sources.Constant const(k=100)
    annotation (Placement(transformation(extent={{-66,32},{-46,52}})));
  Appliances.Showers.ClassicShower Shower
    annotation (Placement(transformation(extent={{16,-4},{36,16}})));
  Blocks.Sources.hydrographFromFile ShowerHyd(fileName=
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://WaterHub/Examples/TestInputs/Shower_tmp.csv"))
    annotation (Placement(transformation(extent={{-4,22},{16,42}})));
equation
  connect(energy.outlet[1], waterSourceHeatPump.ComEnergy_in) annotation (
    Line(points={{-110,20},{-30,20},{-30,12}},                   color = {208, 52, 5}));

  connect(const.y, waterSourceHeatPump.T_set) annotation (Line(points={{-45,42},
          {-36,42},{-36,30},{-24,30},{-24,12.6}}, color={0,0,127}));
  connect(Water.outlets[1], waterSourceHeatPump.port_in_a) annotation (Line(
        points={{-108,-32},{-90,-32},{-90,8},{-34,8}}, color={0,127,255}));
  connect(waterSourceHeatPump.port_out_a, Shower.inletHot)
    annotation (Line(points={{-14,8},{16,8}}, color={0,127,255}));
  connect(Shower.outlet, Wastewater.inlet[1])
    annotation (Line(points={{36,6},{100,6}}, color={0,127,255}));
  connect(Water2.outlets[1], Shower.inletCold) annotation (Line(points={{100,
          -30},{74,-30},{74,-18},{10,-18},{10,4},{16,4}}, color={0,127,255}));
  connect(Water2.outlets[1], waterSourceHeatPump.port_in_b) annotation (Line(
        points={{100,-30},{-8,-30},{-8,-4},{-14,-4}}, color={0,127,255}));
  connect(waterSourceHeatPump.port_out_b, Wastewater.inlet[1]) annotation (Line(
        points={{-34,-4},{-38,-4},{-38,-44},{90,-44},{90,6},{100,6}}, color={0,
          127,255}));
  connect(ShowerHyd.y, Shower.flowInput)
    annotation (Line(points={{17,32},{26,32},{26,15.6}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<p>The example and the description for the heat pump will be updated soon.</p>
</html>"));
end HeatPumpExample;
