within WaterHub.Examples;
model CompleteSystemExample
  extends Modelica.Icons.Example;
  WaterHub.Blocks.Sources.hydrographFromFile ShowerHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Shower_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-14, 136}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.Showers.ClassicShower Shower annotation (
    Placement(visible = true, transformation(origin = {10, 120}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Imports.WaterSupplyMO Water(T = 294.45, n = 10) annotation (
    Placement(visible = true, transformation(origin = {-170, 134}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Imports.EnergySupplyMO Energy(n = 4) annotation (
    Placement(visible = true, transformation(origin = {-170, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipeShower(
    nbNodes=5,
    pipeLength=5,
    tEnvironment=298.15) annotation (Placement(visible=true, transformation(
        origin={-50,122},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.Appliances.Taps.ClassicTap Tap1 annotation (
    Placement(visible = true, transformation(origin = {10, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.Taps.ClassicTap Tap2 annotation (
    Placement(visible = true, transformation(origin = {10, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFile TapAdultsHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/TapAdults_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-14, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFile TapChildrenHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/TapChildren_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-14, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipeTapAdults(
    nbNodes=5,
    pipeLength=5,
    tEnvironment=298.15) annotation (Placement(visible=true, transformation(
        origin={-50,42},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipeTapChildren(
    nbNodes=5,
    pipeLength=5,
    tEnvironment=298.15) annotation (Placement(visible=true, transformation(
        origin={-50,2},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.ImportExport.Exports.WaterSinkMI Wastewater(n = 1) annotation (
    Placement(visible = true, transformation(origin={175,39},    extent={{-15,-15},
            {15,15}},                                                                             rotation = 0)));
  WaterHub.ImportExport.Exports.HeatSinkMI HeatSink(n = 7) annotation (
    Placement(visible = true, transformation(origin = {-50, -164}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipeBath(
    nbNodes=5,
    pipeLength=5,
    tEnvironment=298.15) annotation (Placement(visible=true, transformation(
        origin={-50,82},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.Appliances.Showers.ClassicShower Bath annotation (
    Placement(visible = true, transformation(origin = {10, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFile BathHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Bath_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-14, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipeTapHousehold(
    nbNodes=5,
    pipeLength=5,
    tEnvironment=298.15) annotation (Placement(visible=true, transformation(
        origin={-50,-38},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.Blocks.Sources.hydrographFromFile TapHouseholdHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/TapHousehold_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-14, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.Taps.ClassicTap Tap3 annotation (
    Placement(visible = true, transformation(origin = {10, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.ColdPlumbed.ColdPlumbedApplianceWithOpEnergy Dishes annotation (
    Placement(visible = true, transformation(origin = {-10, -136}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.ColdPlumbed.ColdPlumbedApplianceWithOpEnergy Clothes annotation (
    Placement(visible = true, transformation(origin = {-10, -96}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFile KettleHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Kettle_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {46, -104}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFile ToiletHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Toilet_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {46, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Appliances.Toilet                      Toilet annotation (
    Placement(visible = true, transformation(origin = {70, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Appliances.ColdPlumbed.ColdPlumbedAppliance Kettle annotation (
    Placement(visible = true, transformation(origin = {70, -120}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.ImportExport.Exports.WaterSinkMI HumanSink annotation (
    Placement(visible = true, transformation(origin = {116, -120}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.DHWTechnologies.Boilers.TankBoiler Boiler(
    cop=1,
    loss=51.2,
    lowSetPointT=328.0,
    maxPower=50000,
    n=5,
    setPointT=328.15,
    volume=400) annotation (Placement(visible=true, transformation(
        origin={-92,2},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvMISO
    pipe(
    material=WaterHub.Constants.MaterialConstants.Material.PEX,
    n=8,
    nbNodes=4,
    pipeLength=2) annotation (Placement(visible=true, transformation(
        origin={134,64},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  WaterHub.Blocks.Sources.hydrographFromFileWithOpEnergy DishwasherHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Dishwasher_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-34, -124}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  WaterHub.Blocks.Sources.hydrographFromFileWithOpEnergy WashingMachineHyd(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/WashingMachine_tmp.txt")) annotation (
    Placement(visible = true, transformation(origin = {-32, -78}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(Water.outlets[8], Toilet.inlet) annotation (
    Line(points={{-160,136},{-134,136},{-134,-82},{60,-82},{60,-80}},            color = {0, 127, 255}, thickness = 0.5));
  connect(Dishes.outlet, pipe.inlet[8]) annotation (
    Line(points={{0,-136},{116,-136},{116,64},{124,64},{124,65.75}},         color = {0, 127, 255}));
  connect(Clothes.outlet, pipe.inlet[7]) annotation (
    Line(points={{0,-96},{116,-96},{116,64},{124,64},{124,65.25}},         color = {0, 127, 255}));
  connect(Toilet.outlet, pipe.inlet[6]) annotation (
    Line(points={{80,-80},{116,-80},{116,64},{124,64},{124,64.75}},         color = {0, 127, 255}));
  connect(Tap3.outlet, pipe.inlet[5]) annotation (
    Line(points={{20,-40},{116,-40},{116,64},{124,64},{124,64.25}},         color = {0, 127, 255}));
  connect(Tap2.outlet, pipe.inlet[4]) annotation (
    Line(points={{20,0},{116,0},{116,64},{124,64},{124,63.75}},         color = {0, 127, 255}));
  connect(Tap1.outlet, pipe.inlet[3]) annotation (
    Line(points={{20,40},{116,40},{116,64},{124,64},{124,63.25}},         color = {0, 127, 255}));
  connect(Bath.outlet, pipe.inlet[2]) annotation (
    Line(points={{20,80},{116,80},{116,64},{124,64},{124,62.75}},         color = {0, 127, 255}));
  connect(pipe.heatOutlet, HeatSink.inlet[7]) annotation (
    Line(points={{134,60.5},{134,60.5},{134,-152},{-48,-152},{-48,-154},{
          -46.5714,-154}},                                                                    color = {208, 52, 5}));
  connect(pipe.outlet, Wastewater.inlet[1]) annotation (
    Line(points={{144,64},{140,64},{140,38},{160,38},{160,39}},            color = {0, 127, 255}));
  connect(Shower.outlet, pipe.inlet[1]) annotation (
    Line(points={{20,120},{116,120},{116,64},{124,64},{124,62.25}},         color = {0, 127, 255}));
  connect(DishwasherHyd.y, Dishes.flowInput) annotation (
    Line(points={{-23,-124},{-10,-124},{-10,-126.4},{-10,-126.4}},      color = {0, 0, 127}, thickness = 0.5));
  connect(Energy.outlet[3], Dishes.energy_in) annotation (
    Line(points={{-160,19},{-126,19},{-126,-134},{-20,-134}},          color = {127, 0, 0}, thickness = 0.5));
  connect(Water.outlets[10], Dishes.inlet) annotation (
    Line(points={{-160,137.6},{-134,137.6},{-134,-140},{-20,-140},{-20,-138}},        color = {0, 127, 255}, thickness = 0.5));
  connect(WashingMachineHyd.y, Clothes.flowInput) annotation (
    Line(points={{-21,-78},{-10,-78},{-10,-86.4},{-10,-86.4}},      color = {0, 0, 127}, thickness = 0.5));
  connect(Boiler.heatOutlet, HeatSink.inlet[6]) annotation (
    Line(points={{-92,-8},{-92,-152},{-51,-152},{-51,-156},{-54.5,-156},{-54.5,
          -154},{-47.7143,-154}},                                                                                color = {208, 52, 5}));
  connect(pipeShower.heatOutlet, HeatSink.inlet[1]) annotation (
    Line(points={{-50,118.5},{-50,-18},{-50,-154},{-53.4286,-154}},
                                             color = {208, 52, 5}));
  connect(pipeBath.heatOutlet, HeatSink.inlet[2]) annotation (
    Line(points={{-50,78.5},{-50,-38},{-50,-154},{-52.2857,-154}},
                                            color = {208, 52, 5}));
  connect(pipeTapAdults.heatOutlet, HeatSink.inlet[3]) annotation (
    Line(points={{-50,38.5},{-50,-58},{-50,-154},{-51.1429,-154}},
                                            color = {208, 52, 5}));
  connect(pipeTapChildren.heatOutlet, HeatSink.inlet[4]) annotation (
    Line(points={{-50,-1.5},{-50,-154}},    color = {208, 52, 5}));
  connect(pipeTapHousehold.heatOutlet, HeatSink.inlet[5]) annotation (
    Line(points={{-50,-41.5},{-50,-98},{-50,-154},{-48.8571,-154}},
                                             color = {208, 52, 5}));
  connect(Kettle.outlet, HumanSink.inlet[1]) annotation (
    Line(points={{80,-120},{106,-120}},      color = {0, 127, 255}));
  connect(ShowerHyd.y, Shower.flowInput) annotation (
    Line(points={{-3,136},{10,136},{10,129.6}},      color = {0, 0, 127}, thickness = 0.5));
  connect(Water.outlets[2], Shower.inletCold) annotation (
    Line(points={{-160,131.2},{-30,131.2},{-30,118},{0,118},{0,118}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Water.outlets[4], Tap1.inletCold) annotation (
    Line(points={{-160,132.8},{-30,132.8},{-30,38},{0,38},{0,38}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Water.outlets[5], Tap2.inletCold) annotation (
    Line(points={{-160,133.6},{-30,133.6},{-30,-2},{0,-2},{0,-2}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Water.outlets[6], Tap3.inletCold) annotation (
    Line(points={{-160,134.4},{-30,134.4},{-30,-42},{0,-42},{0,-42}},        color = {0, 127, 255}, thickness = 0.5));
  connect(ToiletHyd.y, Toilet.flowInput) annotation (
    Line(points={{57,-64},{70,-64},{70,-70.4}},      color = {0, 0, 127}, thickness = 0.5));
  connect(KettleHyd.y, Kettle.flowInput) annotation (
    Line(points={{57,-104},{70,-104},{70,-110.4}},      color = {0, 0, 127}, thickness = 0.5));
  connect(Water.outlets[7], Clothes.inlet) annotation (
    Line(points={{-160,135.2},{-134,135.2},{-134,-98},{-20,-98},{-20,-98}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Water.outlets[9], Kettle.inlet) annotation (
    Line(points={{-160,136.8},{-134,136.8},{-134,-122},{60,-122},{60,-122}},        color = {0, 127, 255}, thickness = 0.5));
  connect(BathHyd.y, Bath.flowInput) annotation (
    Line(points={{-3,98},{10,98},{10,89.6}},      color = {0, 0, 127}, thickness = 0.5));
  connect(TapAdultsHyd.y, Tap1.flowInput) annotation (
    Line(points={{-3,58},{10,58},{10,49.6}},      color = {0, 0, 127}, thickness = 0.5));
  connect(TapChildrenHyd.y, Tap2.flowInput) annotation (
    Line(points={{-3,18},{10,18},{10,9.6}},       color = {0, 0, 127}, thickness = 0.5));
  connect(TapHouseholdHyd.y, Tap3.flowInput) annotation (
    Line(points={{-3,-24},{10,-24},{10,-30.4}},      color = {0, 0, 127}, thickness = 0.5));
  connect(Energy.outlet[1], Boiler.energy_in) annotation (
    Line(points={{-160,15},{-92,15},{-92,12},{-92,12}},          color = {127, 0, 0}, thickness = 0.5));
  connect(Water.outlets[1], Boiler.inlet) annotation (
    Line(points={{-160,130.4},{-114,130.4},{-114,2},{-102,2},{-102,2}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Boiler.outlet[1], pipeShower.inlet) annotation (
    Line(points={{-82,-1.2},{-74,-1.2},{-74,122},{-60,122},{-60,122}},      color = {0, 127, 255}, thickness = 0.5));
  connect(Boiler.outlet[2], pipeBath.inlet) annotation (
    Line(points={{-82,0.4},{-74,0.4},{-74,82},{-60,82},{-60,82}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Boiler.outlet[3], pipeTapAdults.inlet) annotation (
    Line(points = {{-82, 2}, {-74, 2}, {-74, 42}, {-60, 42}, {-60, 42}}, color = {0, 127, 255}, thickness = 0.5));
  connect(Boiler.outlet[4], pipeTapChildren.inlet) annotation (
    Line(points={{-82,3.6},{-60,3.6},{-60,2},{-60,2}},      color = {0, 127, 255}, thickness = 0.5));
  connect(Boiler.outlet[5], pipeTapHousehold.inlet) annotation (
    Line(points={{-82,5.2},{-74,5.2},{-74,-38},{-60,-38},{-60,-38}},        color = {0, 127, 255}, thickness = 0.5));
  connect(Water.outlets[3], Bath.inletCold) annotation (
    Line(points={{-160,132},{-30,132},{-30,78},{0,78},{0,78}},            color = {0, 127, 255}, thickness = 0.5));
  connect(pipeBath.outlet, Bath.inletHot) annotation (
    Line(points = {{-40, 82}, {0, 82}, {0, 82}, {0, 82}}, color = {0, 127, 255}));
  connect(Energy.outlet[4], Kettle.energy_in) annotation (
    Line(points={{-160,21},{-126,21},{-126,-118},{60,-118}},          color = {127, 0, 0}, thickness = 0.5));
  connect(Energy.outlet[2], Clothes.energy_in) annotation (
    Line(points={{-160,17},{-126,17},{-126,-94},{-20,-94}},          color = {127, 0, 0}, thickness = 0.5));
  connect(pipeTapHousehold.outlet, Tap3.inletHot) annotation (
    Line(points = {{-40, -38}, {0, -38}, {0, -38}, {0, -38}}, color = {0, 127, 255}));
  connect(pipeTapChildren.outlet, Tap2.inletHot) annotation (
    Line(points = {{-40, 2}, {0, 2}}, color = {0, 127, 255}));
  connect(pipeTapAdults.outlet, Tap1.inletHot) annotation (
    Line(points = {{-40, 42}, {0, 42}, {0, 42}, {0, 42}}, color = {0, 127, 255}));
  connect(pipeShower.outlet, Shower.inletHot) annotation (
    Line(points = {{-40, 122}, {0, 122}, {0, 122}, {0, 122}}, color = {0, 127, 255}));
  annotation (
    Icon(coordinateSystem(extent = {{-150, -150}, {150, 150}})),
    Diagram(coordinateSystem(extent = {{-150, -150}, {150, 150}})),
    version = "",
    uses,
    __OpenModelica_commandLineOptions = "",
    Documentation(info="<html>
<p>This example presents a case of a typical household water connection that has hot water (328K or 55oC) and cold water (294.45K or 21oC) supply to a number of appliances. Hot water from a boiler is conveyed by pipes that lose a fraction of heat to the surrounding environment. </p>
<p>The used water from each of these appliances (barring the &apos;Kettle&apos;) is conveyed together to an outlet (called &apos;Wastewater&apos; in the model) via an outlet pipe that, too, loses heat to the surrounding environment. </p>
</html>"));
end CompleteSystemExample;
