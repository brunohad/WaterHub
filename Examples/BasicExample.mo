within WaterHub.Examples;
model BasicExample
  extends Modelica.Icons.Example;
  ImportExport.Imports.WaterSupplyMO waterSupplyMO(n=2)
    annotation (Placement(transformation(extent={{-124,-10},{-104,10}})));
  ImportExport.Imports.EnergySupplyMO energySupplyMO
    annotation (Placement(transformation(extent={{-124,28},{-104,48}})));
  Appliances.Showers.ClassicShower Shower
    annotation (Placement(transformation(extent={{8,4},{28,24}})));
  DHWTechnologies.Boilers.TankBoiler tankBoiler
    annotation (Placement(transformation(extent={{-76,6},{-56,26}})));
  DHWTechnologies.Treatment.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO
    pipe annotation (Placement(transformation(extent={{-34,6},{-14,26}})));
  ImportExport.Exports.WaterSinkSI waterSinkSI
    annotation (Placement(transformation(extent={{56,4},{76,24}})));
  ImportExport.Exports.HeatSinkMI heatSinkMI(n=2)
    annotation (Placement(transformation(extent={{-20,-28},{0,-8}})));
  Blocks.Sources.hydrographFromFile ShowerHyd(fileName=
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://WaterHub/Examples/TestInputs/ShowerTestInput.csv"))
    annotation (Placement(transformation(extent={{-8,32},{12,52}})));
equation
  connect(energySupplyMO.outlet[1], tankBoiler.energy_in)
    annotation (Line(points={{-104,38},{-66,38},{-66,26}}, color={127,0,0}));
  connect(waterSupplyMO.outlets[1], tankBoiler.inlet) annotation (Line(points={
          {-104,-2},{-90,-2},{-90,16},{-76,16},{-76,16}}, color={0,127,255}));
  connect(waterSupplyMO.outlets[2], Shower.inletCold) annotation (Line(points={
          {-104,2},{-98,2},{-98,-2},{0,-2},{0,12},{8,12}}, color={0,127,255}));
  connect(tankBoiler.outlet[1], pipe.inlet)
    annotation (Line(points={{-56,16},{-34,16}}, color={0,127,255}));
  connect(pipe.outlet, Shower.inletHot)
    annotation (Line(points={{-14,16},{8,16}}, color={0,127,255}));
  connect(tankBoiler.heatOutlet, heatSinkMI.inlet[1])
    annotation (Line(points={{-66,6},{-66,-20},{-20,-20}}, color={208,52,5}));
  connect(pipe.heatOutlet, heatSinkMI.inlet[2]) annotation (Line(points={{-24,
          12.5},{-24,-16},{-20,-16}}, color={208,52,5}));
  connect(Shower.outlet, waterSinkSI.inlet)
    annotation (Line(points={{28,14},{56,14}}, color={0,127,255}));
  connect(ShowerHyd.y, Shower.flowInput)
    annotation (Line(points={{13,42},{18,42},{18,23.6}}, color={0,0,127}));
  annotation (
    __OpenModelica_simulationFlags(lv = "LOG_STATS", outputFormat = "mat", s = "dassl"),
      Documentation(info="<html>
<p>This example presents a simple case of convective heat loss in pipe connections. Hot water from tank boiler is conveyed to the shower via a pipe that loses heat to the environment, which is described by the thermal conductance of the pipe, which, in turn, is regulated by the temperature of the surrounding environment and pipe dimensions (Hadengue <i>et al</i>., 2020). These parameters can be defined by the user in the pipe model interface <b>(Fig. 1</b>).</p>
<p align=\"center\"><img src=\"modelica://WaterHub/../WH tutorial figures/BasicExample_4.png\"/></p>
<p align=\"center\"><b>Fig. 1</b>: Parameters in the pipe model that govern heat loss to the surrounding environment</p>
<h4>Parameters descrption</h4>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"20%\"><tr>
<td><h4>Parameters</h4></td>
<td><h4>Description</h4></td>
</tr>
<tr>
<td><p>nbNodes</p></td>
<td><p>the number of pipe nodes that represent spatial discretisation</p></td>
</tr>
<tr>
<td><p>pipeLength</p></td>
<td><p>Length of the pipe in metres</p></td>
</tr>
<tr>
<td><p>pipeDiameterInside</p></td>
<td><p>Inside diameter of pipe in metres</p></td>
</tr>
<tr>
<td><p>pipeThickness</p></td>
<td><p>Thickness of pipe wall in metres</p></td>
</tr>
<tr>
<td><p>material</p></td>
<td><p>Pipe material type</p></td>
</tr>
<tr>
<td><p>tEnvironment</p></td>
<td><p>Temperature of the surrounding environment</p></td>
</tr>
<tr>
<td><p>staticConv</p></td>
<td></td>
</tr>
<tr>
<td><p><br>airConv</p></td>
<td></td>
</tr>
</table>
<p><br><h4>References</h4></p>
<p>Hadengue, B., <i>et al.</i> (2020). Modeling the Water-Energy Nexus in Households. Submitted.</p>
</html>"));
end BasicExample;
