within WaterHub.Appliances;
package Taps
  extends Modelica.Icons.Package;
  import SI=WaterHub.SIUnits;
  import CO = WaterHub.Constants;

  model ClassicTap
    extends WaterHub.Appliances.BaseAppliances.BasicValve;
  parameter Real reduceFlowFactor(min=0.0, max=1.0) = 1.0;
  
  equation
    T_wanted = smooth(0, flowInput[2]+273.15);
    -T_achieved*outlet.water = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = smooth(0, flowInput[1]*reduceFlowFactor) "Mass balance";
    outlet.water + inletCold.water + inletHot.water = 0 "Flow balance";
    outlet.T = T_achieved;

    annotation (defaultComponentName="Tap", Icon(graphics={Bitmap(extent={{-78,
                -70},{76,80}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/Tap.bmp")}));
  end ClassicTap;

  model FlowModifyingTap "Valve that takes a third argument from the flowInput port: the required dT against the cold water input temperature. It extends the flow such that the flow duration 'starts' once the required dT is attained. Attention: change 'columns' to {2,3,4} in HydrographFromFile"
    extends WaterHub.Icons.ModelIcon;
    SI.AbsoluteTemperature T_wanted "Target Temperature";
    SI.AbsoluteTemperature T_achieved "Achieved Temperature";
  Boolean TAdaptHot(start=false) "Boolean for T_wanted VS T_achieved choice";
    Boolean TAdaptCold(start=false) "Boolean for T_wanted VS T_achieved choice";
    Real TAdaptHotReal(start=0.0);
    Real TAdaptColdReal(start=0.0);

    //inlet ports
    WaterHub.BaseClasses.WaterPort_in inletCold(water(min=-10e-5))
    annotation (Placement(transformation(extent={{-110,-30},{-90,-10}})));
    WaterHub.BaseClasses.WaterPort_in inletHot(water(min=-10e-5))
    annotation (Placement(transformation(extent={{-110,10},{-90,30}})));
    Modelica.Blocks.Interfaces.RealInput[3] flowInput "flowInput[1] is flow, flowInput[2] is demanded temperature, flowInput[3] is minimal temp difference with respect to incoming cold water"
    annotation (Placement(transformation(extent={{-10,90},{10,110}}, rotation=-90)));

    //outlet ports
    WaterHub.BaseClasses.WaterPort_out outlet(water(start= 0.0, max=0))
    annotation (Placement(transformation(extent={{110,-10},{90,10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet(heat(start = 0.0)) "Heat outlet port to quantify wasted energy (useless energy because not meeting dT demand)" annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));

    Boolean event(start = false);
    Boolean wastedEnergy(start= false);
    discrete Real tStart(start = 0.0);
    discrete Real tReached(start = 100.0);
    discrete Real tFinal(start= 0.0);
    discrete Real tEnd(start= 0.0);
    SI.WaterFlow realFlow(start=0.0);
    discrete SI.WaterFlow eventFlow(start=0.0);
    discrete SI.AbsoluteTemperature eventTemp(start=0.0);
    discrete SI.AbsoluteTemperature eventdT(start=298.15);

  algorithm

    when event then
      tStart := time;
      if (inletHot.T < eventdT) then
    tReached := 100;
  else
    tReached := 0;
  end if;
    end when;
    when (time > tStart+3 and event) then
      eventFlow := flowInput[1];
      eventTemp := flowInput[2]+273.15;
      eventdT := flowInput[3]+283.15;
    end when;
    when not event then
  tEnd := time;
  tFinal := (tEnd+tReached);
    end when;

    when (inletHot.T > eventdT) then
  tReached := time-tStart;
  if (not event) then
    tFinal := (tEnd+tReached);
  else
    tFinal := pre(tFinal);
  end if;
    end when;

  equation
    TAdaptCold = T_wanted < inletCold.T;
    TAdaptColdReal = if TAdaptCold then 1.0 else 0.0;
    TAdaptHot = T_wanted > inletHot.T;
    TAdaptHotReal = if TAdaptHot then 1.0 else 0.0;
    T_achieved = TAdaptHotReal * inletHot.T + TAdaptColdReal * inletCold.T + (1 - TAdaptColdReal)*(1 - TAdaptHotReal)*T_wanted;


    //T_wanted = flowInput[2]+273.15;
    -T_achieved*outlet.water = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = realFlow "Mass balance";
    outlet.water + inletCold.water + inletHot.water = 0 "Flow balance";
    outlet.T = T_achieved;

    event = flowInput[1] > 0.001;

    //continues flow until the dT difference has been hold for long enough
    if event then
  realFlow = flowInput[1];
  T_wanted = flowInput[2]+273.15;
    elseif (not event and time < tFinal) then
  realFlow = eventFlow;
  T_wanted = eventTemp;
    else
  realFlow = 0;
  T_wanted = 0;
    end if;

    //wastedEnergy through heat port
    wastedEnergy = T_achieved < flowInput[3]+283.15;
    if wastedEnergy then
  heatOutlet.heat + realFlow*(T_achieved-inletCold.T)*CO.VolSpecificHeatCapWater = 0;
    else
  heatOutlet.heat = 0;
    end if;

    annotation (defaultComponentName="FlowModifyingTap", Documentation(info="<html>
<p>Valve that takes a third argument from the flowInput port: the required dT against the cold water input temperature. It extends the flow such that the flow duration &apos;starts&apos; once the required dT is attained. Attention: change &apos;columns&apos; to {2,3,4} in HydrographFromFile</p>
</html>"));
  end FlowModifyingTap;

  annotation (Icon(graphics={Bitmap(extent={{-88,-92},{90,90}}, fileName=
              "modelica://WaterHub/Resources/Images/Icons/Tap.bmp")}));
end Taps;
