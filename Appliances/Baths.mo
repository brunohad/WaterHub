within WaterHub.Appliances;
package Baths
  extends Modelica.Icons.Package;
  import SI=WaterHub.SIUnits;
  import CO = WaterHub.Constants;

  model ClassicBath
    extends WaterHub.Appliances.BaseAppliances.BasicValve;
    SI.Volume volume(start=0.0) "Bath volume";
    parameter SI.WaterFlow maxFlow = 0.5 "Maximal flow through outlet";
    Boolean event(start = false);
    parameter Real bathTime = 180 "waiting time before emptying the pipes";
    final constant Real triggerValue = 0.001;
    discrete Real t(start = 0.0);

  algorithm
            //for some reason, JModelica does not like to have the algorithm part in the BaseShower partial model.

    if noEvent(T_wanted < inletCold.T) then   //Make sure wanted T is inside the boundary set by Hot and Cold Water
      T_achieved := inletCold.T;
    elseif noEvent(T_wanted > inletHot.T) then
      T_achieved := inletHot.T;
    else
      T_achieved := T_wanted;
    end if;

  equation

    event = inlet.water > triggerValue "Careful, as a too high triggerValue leads to 'Chattering' problems";
    when change(event) then
      t = time;
    end when;

    T_wanted = smooth(0, flowInput[2]+273.15) "conversion to Kelvin (flowInput[2] is in Celsius). T_wanted is 273.15 K = 0 C";
    -T_achieved*volume = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = smooth(0, flowInput[1]) "Demand equation";


    if event then
  -der(volume) + inletCold.water + inletHot.water = 0 "Flow balance";
  outlet.water = 0;
    else
  der(volume)= -maxFlow;
    end if;

    outlet.T = T_achieved;

    annotation (defaultComponentName="Bath");
  end ClassicBath;


  annotation (Documentation(info="<html>
<p>Extends from WaterHub.Appliances.BaseAppliances.BasicValve (partial model that has two water inlets (hot, cold), one outlet and one data input connector (containing the demanded flow and temperature information).</p>
</html>"));

end Baths;
