within WaterHub.Appliances;
package Showers
  extends Modelica.Icons.Package;
  import SI=WaterHub.SIUnits;
  import CO = WaterHub.Constants;

  partial model BaseShower "A base shower model in which the basic algorithm for achieved temperature is defined"
    extends WaterHub.Appliances.BaseAppliances.BasicValve;

    annotation (Icon(graphics={  Bitmap(extent = {{-66, -78}, {72, 90}}, fileName = "modelica://WaterHub/Resources/Images/Icons/Shower.bmp")}, coordinateSystem(initialScale = 0.1)), Documentation(info="<html>
<p>A base shower model in which the basic algorithm for achieved temperature is defined</p>
</html>"));

  end BaseShower;

  model ClassicShower "Shower model that takes an external hydrograph as input values"
    extends BaseShower;
  parameter Real reduceFlowFactor(min=0.0, max=1.0) = 1.0;
  
  equation
    T_wanted = smooth(0, flowInput[2]+273.15) "conversion to Kelvin (flowInput[2] is in Celsius). T_wanted is 273.15 K = 0 C";

    -T_achieved*outlet.water = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = smooth(0, flowInput[1]* reduceFlowFactor) "Demand equation";
    outlet.water + inletCold.water + inletHot.water = 0 "Flow balance";
    outlet.T = T_achieved;

    annotation (defaultComponentName="Shower", Documentation(info="<html>
<p>Shower model that takes an external hydrograph as input values</p>
</html>"));
  end ClassicShower;




  model LossShowerLinear
    "Shower model that takes an external hydrograph as input values and loses heat to the surrounding environment linearly"

    extends BaseShower;
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation (
      Placement(transformation(extent = {{-10, -110}, {10, -90}})));
    //parameters
    parameter Real heatLossFactor = 0.05 "Lost energy during shower, should be between 0 and 1";
    parameter SI.AbsoluteTemperature T_coldWater = 283.15 "Should correspond to reference cold water in the system (T of water import)";
  equation
    T_wanted = smooth(0, flowInput[2]+273.15) "conversion to Kelvin (flowInput[2] is in Celsius). T_wanted is 273.15 K = 0 C";

    -T_achieved*outlet.water = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = smooth(0, flowInput[1]) "Demand equation";
    outlet.water + inletCold.water + inletHot.water = 0 "Flow balance";

    outlet.T = T_achieved - heatLossFactor*(T_achieved-T_coldWater);
    heatOutlet.heat = heatLossFactor*(T_achieved-T_coldWater)*CO.VolSpecificHeatCapWater*outlet.water;

    annotation (defaultComponentName="Shower", Documentation(info="<html>
  <p>Shower model that takes an external hydrograph as input values and loses heat to the surrounding environment using a linear relationship</p>
</html>"));
  end LossShowerLinear;

  model LossShowerWong
    "Shower model that takes an external hydrograph as input values and loses heat to the surrounding environment according to the empirical formula developed by Wong et al. (2010)"

    extends BaseShower;
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation (
      Placement(transformation(extent = {{-10, -110}, {10, -90}})));
    SI.TemperatureDifference deltaT "Temperature difference faucet-drain";
    //parameters
    parameter SI.AbsoluteTemperature T_surroundingAir = 273.15 + 23 "Should correspond to reference cold water in the system (T of water import)";
    parameter Real reduceFlowFactor(min=0.0, max=1.0) = 1.0;
  equation
    T_wanted = smooth(0, flowInput[2]+273.15) "conversion to Kelvin (flowInput[2] is in Celsius). T_wanted is 273.15 K = 0 C";

    -T_achieved*outlet.water = inletCold.T*inletCold.water+inletHot.T*inletHot.water "Energy balance";
    inletCold.water + inletHot.water = smooth(0, flowInput[1]*reduceFlowFactor) "Demand equation";
    outlet.water + inletCold.water + inletHot.water = 0 "Flow balance";

    deltaT = 3.6*10^(-10)*(T_achieved-273.15)^6.673*(T_surroundingAir-273.15)^(-0.530) "Wong et al. empirical formula for temperature loss";
    outlet.T = T_achieved - deltaT;
    heatOutlet.heat = deltaT*CO.VolSpecificHeatCapWater*outlet.water;

    annotation (defaultComponentName="Shower", Documentation(info="<html>
<p>Shower model that takes an external hydrograph as input values and loses heat to the surrounding environment according to the empirical formula developed by Wong et al. (2010).</p>
<p><br>Wong, L. T., Mui, K. W. &amp; Guan, Y. 2010 Shower water heat recovery in high-rise residential buildings of Hong Kong. Applied Energy 87 (2), 703&ndash;709.</p>
</html>"));
  end LossShowerWong;
  annotation (Icon(graphics={  Bitmap(extent = {{-88, -96}, {84, 92}}, fileName = "modelica://WaterHub/Resources/Images/Icons/Shower.bmp")}, coordinateSystem(initialScale = 0.1)));
end Showers;
