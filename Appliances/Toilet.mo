within WaterHub.Appliances;
model Toilet
  extends WaterHub.Icons.ModelIcon;
  //inlet ports
  WaterHub.BaseClasses.WaterPort_in inlet(water(min = -10e-5)) annotation (
    Placement(visible = true, transformation(extent = {{-110, -10}, {-90, 10}}, rotation = 0), iconTransformation(extent = {{-110, -10}, {-90, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput[2] flowInput "flowInput[1] is flow, flowInput[2] is demanded temperature" annotation (
    Placement(transformation(extent = {{-10, 90}, {10, 110}}, rotation = -90,
        origin={-100,96}), iconTransformation(
        extent={{-10,90},{10,110}},
        rotation=-90,
        origin={-100,96})));
  //outlet port
  WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation (
    Placement(transformation(extent={{110,-10},{90,10}}), iconTransformation(
          extent={{110,-10},{90,10}})));
equation
  connect(inlet, outlet) annotation (
    Line(points={{-100,0},{100,0},{100,0},{100,0}}));
  inlet.water = smooth(0, flowInput[1]) "Demand equation";
  annotation (Icon(graphics={Bitmap(extent={{-80,-74},{80,80}}, fileName=
              "modelica://WaterHub/Resources/Images/Icons/Toilet.bmp")}));
end Toilet;
