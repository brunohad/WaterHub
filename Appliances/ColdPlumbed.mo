within WaterHub.Appliances;
package ColdPlumbed
  extends Modelica.Icons.Package;
  import SI=WaterHub.SIUnits;
  import CO = WaterHub.Constants;

  model ColdPlumbedAppliance
    extends WaterHub.Icons.ModelIcon;

    //inlet ports
    WaterHub.BaseClasses.HeatPort_in energy_in(heat(min = -1e-5)) annotation (
      Placement(visible = true, transformation(extent = {{-110, 10}, {-90, 30}}, rotation = 0), iconTransformation(extent = {{-110, 10}, {-90, 30}}, rotation = 0)));
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -10e-5)) annotation (
      Placement(visible = true, transformation(extent = {{-110, -30}, {-90, -10}}, rotation = 0), iconTransformation(extent = {{-110, -30}, {-90, -10}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealInput[2] flowInput "flowInput[1] is flow, flowInput[2] is demanded temperature"
    annotation (Placement(transformation(extent={{-10,90},{10,110}}, rotation=-90,
          origin={-100,96}), iconTransformation(
          extent={{-10,90},{10,110}},
          rotation=-90,
          origin={-100,96})));

    //outlet port
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation (
  Placement(transformation(extent={{110,-10},{90,10}})));
  equation
    outlet.water * (flowInput[2]+273.15) + inlet.T * inlet.water + energy_in.heat / CO.VolSpecificHeatCapWater = 0 "energy balance";
    inlet.water = flowInput[1] "Demand equation";
    outlet.water + inlet.water = 0 "mass balance";
    outlet.T = (flowInput[2] +273.15);

    annotation (Icon(graphics={Bitmap(extent={{-80,-64},{80,80}}, fileName="modelica://WaterHub/Resources/Images/Icons/ColdPlumbed.bmp")}), Documentation(info="<html>
<p>Cold plumbed appliance: any appliance that is plumbed to cold water only, and heats up water internally: washing machines, dishwashers, kettle, etc.</p>
</html>"));
  end ColdPlumbedAppliance;

  model ColdPlumbedApplianceWithOpEnergy
    extends WaterHub.Icons.ModelIcon;
    //inlet ports
    //Note concerning energy_in >> it is slightly confusing to have energy_in as a heat port, although strictly not heat is going through that port, only electricity (for water heating and operation).
    WaterHub.BaseClasses.HeatPort_in energy_in(heat(min = -1e-5)) annotation (
      Placement(visible = true, transformation(extent = {{-110, 10}, {-90, 30}}, rotation = 0), iconTransformation(extent = {{-110, 10}, {-90, 30}}, rotation = 0)));
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -10e-5)) annotation (
      Placement(visible = true, transformation(extent = {{-110, -30}, {-90, -10}}, rotation = 0), iconTransformation(extent = {{-110, -30}, {-90, -10}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealInput[3] flowInput "flowInput[1] is flow, flowInput[2] is demanded temperature, flowInput[3] is required operational energy"
    annotation (Placement(transformation(extent={{-10,90},{10,110}}, rotation=-90,
          origin={-100,96}), iconTransformation(
          extent={{-10,90},{10,110}},
          rotation=-90,
          origin={-100,96})));
    //outlet port
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation (
  Placement(transformation(extent={{110,-10},{90,10}}), iconTransformation(
            extent={{110,-10},{90,10}})));

    //useful variables
    SI.Power waterHeating;
    SI.Power operationEnergy;

  equation
    outlet.water * (flowInput[2]+273.15) + inlet.T * inlet.water + waterHeating / CO.VolSpecificHeatCapWater = 0 "energy balance";
    inlet.water = flowInput[1] "Demand equation";
    outlet.water + inlet.water = 0 "mass balance";
    outlet.T = (flowInput[2] +273.15);
    operationEnergy = flowInput[3]*3.6*1e6 "including conversion to Joules";
    waterHeating + operationEnergy = energy_in.heat;

    annotation (Icon(graphics={Bitmap(extent={{-80,-60},{82,80}}, fileName=
                "modelica://WaterHub/Resources/Images/Icons/ColdPlumbed.bmp")}), Documentation(info="<html>
<p>Cold plumbed appliance: any appliance that is plumbed to cold water only, and heats up water internally: washing machines, dishwashers, kettle, etc. This appliance contains a term for operational energy (pumping, mechanical, etc.), i.e. the input from HydroGen must have 3 columns (the third one being the demanded operational energy in kWh).</p>
</html>"));
  end ColdPlumbedApplianceWithOpEnergy;

  annotation (Icon(graphics = {Bitmap(extent = {{-90, -94}, {90, 98}}, fileName = "modelica://WaterHub/Resources/Images/Icons/ColdPlumbed.bmp")}, coordinateSystem(initialScale = 0.1)));
end ColdPlumbed;
