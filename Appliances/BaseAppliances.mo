within WaterHub.Appliances;
package BaseAppliances
  extends Modelica.Icons.Package;

  partial model BasicValve "partial model that has two water inlets (hot, cold), one outlet and one data input connector (containing the demanded flow and temperature information"
    extends WaterHub.Icons.ModelIcon;

    //inlet ports
    WaterHub.BaseClasses.WaterPort_in inletCold(water(min=-10e-5))
    annotation (Placement(transformation(extent={{-110,-30},{-90,-10}})));
    WaterHub.BaseClasses.WaterPort_in inletHot(water(min=-10e-5))
    annotation (Placement(transformation(extent={{-110,10},{-90,30}})));
    Modelica.Blocks.Interfaces.RealInput[2] flowInput "flowInput[1] is flow, flowInput[2] is demanded temperature"
    annotation (Placement(transformation(extent={{-10,90},{10,110}}, rotation=-90,
          origin={-100,96}), iconTransformation(
          extent={{-10,90},{10,110}},
          rotation=-90,
          origin={-100,96})));

    //outlet ports
    WaterHub.BaseClasses.WaterPort_out outlet(water(max=0))
    annotation (Placement(transformation(extent={{110,-10},{90,10}}),
          iconTransformation(extent={{110,-10},{90,10}})));

    SI.AbsoluteTemperature T_wanted "Target Temperature";
    SI.AbsoluteTemperature T_achieved "Achieved Temperature";
  //  Boolean TAdaptHot(start=false) "Boolean for T_wanted VS T_achieved choice";
  //  Boolean TAdaptCold(start=false) "Boolean for T_wanted VS T_achieved choice";
    Real TAdaptHotReal(start=0.0);
    Real TAdaptColdReal(start=0.0);

  equation
  //  TAdaptCold = smooth(0, noEvent());
    TAdaptColdReal = smooth(0, noEvent(if (T_wanted < inletCold.T) then 1.0 else 0.0));
  //  TAdaptHot = smooth(0, noEvent);
    TAdaptHotReal = smooth(0, noEvent(if (T_wanted > inletHot.T) then 1.0 else 0.0));
    T_achieved = TAdaptHotReal * inletHot.T + TAdaptColdReal * inletCold.T + (1 - TAdaptColdReal)*(1 - TAdaptHotReal)*T_wanted;


  end BasicValve;


end BaseAppliances;
