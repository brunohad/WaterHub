﻿within WaterHub.Conveyance;

package WaterPipes
  extends Modelica.Icons.Package;
  import SI = WaterHub.SIUnits;
  import CO = WaterHub.Constants;
  import BF = WaterHub.BaseClasses.BaseFunctions;

  package ConstantConvection
    extends Modelica.Icons.Package;

    model BasePipeUA "Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
      parameter Integer nbNodes = 1 "Number of nodes";
      parameter SI.PipeLength pipeLength = 29;
      parameter SI.PipeDiameter pipeDiameterInside = 0.0127;
      parameter SI.PipeThickness pipeThickness = 0.00375;
      final parameter SI.PipeDiameter pipeDiameterOutside = pipeDiameterInside + pipeThickness;
      parameter CO.MaterialConstants.Material material = CO.MaterialConstants.Material.Copper;
      parameter SI.AbsoluteTemperature tEnvironment = 293.15;
      parameter Boolean insulationLayer = false annotation(
        Dialog(group = "Insulation"));
      parameter SI.PipeThickness insulationThickness = 0.01 annotation(
        Dialog(group = "Insulation", enable = insulationLayer));
      parameter SI.ThermalConductivity kInsulation = 0.004 annotation(
        Dialog(group = "Insulation", enable = insulationLayer));
      //variables
      SI.Volume volume[nbNodes];
      SI.Volume wallVolume[nbNodes];
      SI.AbsoluteTemperature nodeT[nbNodes](each start = tEnvironment);
      SI.Area areaInside[nbNodes];
      SI.Area areaOutside[nbNodes];
      SI.ThermalConductivity k;
      SI.Density wallDensity;
      SI.VolSpecificHeatCap CWall;
      SI.OverallConductance UA[nbNodes];
      SI.HeatFlow losses[nbNodes];
      //parameters
      parameter SI.hConv staticConv = 100;
      parameter SI.hConv airConv = 30;
    algorithm
      if material == CO.MaterialConstants.Material.Copper then
        k := 400.0 "Values for k are taken from https://www.engineeringtoolbox.com/thermal-conductivity-d_429.html";
        wallDensity := 8940;
        CWall := wallDensity * 0.39 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
      elseif material == CO.MaterialConstants.Material.PEX then
        k := 0.50;
        wallDensity := 940;
        CWall := wallDensity * 0.55 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
      elseif material == CO.MaterialConstants.Material.Steel then
        k := 16;
        wallDensity := 7850;
        CWall := wallDensity * 0.5024 "Density (kg m-3) times specific heat capacity (kJ kg-1 K-1)";
      else
        k := 0.0;
        wallDensity := 0;
        CWall := 0.0;
      end if;
      for i in 1:nbNodes loop
        volume[i] := pipeLength / nbNodes * CO.pi * (pipeDiameterInside / 2) ^ 2 * 1e3 "Volume of each node in Liters. The pipe is divided into nbNodes equal sections";
        wallVolume[i] := pipeLength / nbNodes * CO.pi * (pipeDiameterOutside ^ 2 - pipeDiameterInside ^ 2);
        areaInside[i] := CO.pi * pipeDiameterInside * (pipeLength / nbNodes);
        areaOutside[i] := CO.pi * pipeDiameterOutside * (pipeLength / nbNodes);
        if insulationLayer then
          UA[i] := 1. / (1. / (staticConv * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + log((pipeDiameterOutside + 2 * insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
        else
          UA[i] := 1. / (1. / (staticConv * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
        end if;
      end for;
    equation
// log((pipeDiameterOutside + insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) +
      annotation(
        Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}, fillPattern = FillPattern.Solid, fillColor = {95, 95, 95}, pattern = LinePattern.None), Rectangle(extent = {{-100, 44}, {100, -44}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 127, 255}), Text(extent = {{-90.0, -20.0}, {90.0, 20.0}}, lineColor = {255, 255, 255}, textString = "%name")}));
    end BasePipeUA;

    model ConstantConvSISO
      extends ConstantConvection.BasePipeUA;
      //    annotation(Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}, fillPattern = FillPattern.Solid, fillColor = {95, 95, 95}, pattern = LinePattern.None), Rectangle(extent = {{-100, 44}, {100, -44}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 127, 255})}));
      //inlets
      WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
        Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
      //outlets
      WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
        Placement(transformation(extent = {{110, -10}, {90, 10}})));
      WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
        Placement(transformation(extent = {{-10, -45}, {10, -25}})));
      //parameters
    equation
      for i in 1:nbNodes loop
        losses[i] = UA[i] * (nodeT[i] - tEnvironment);
        if i == 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (inlet.T - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        elseif i <> 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (nodeT[i - 1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        end if;
      end for;
      inlet.water + outlet.water = 0 "flow balance";
      outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
      sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
      annotation(
        defaultComponentName = "pipe");
    end ConstantConvSISO;

    model ConstantConvSIMO
      extends ConstantConvection.BasePipeUA;
      //parameters
      parameter Integer n = 1 "Number of outlet ports";
      //inlets
      WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
        Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
      //outlets
      WaterHub.BaseClasses.WaterPorts_out[n] outlet(each water(max = 1e-5)) annotation(
        Placement(transformation(extent = {{110, -30}, {90, 30}})));
      WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
        Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    equation
      for i in 1:nbNodes loop
        losses[i] = UA[i] * (nodeT[i] - tEnvironment);
// energy balance for each node.
        if i == 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (inlet.T - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        elseif i <> 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (nodeT[i - 1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        end if;
      end for;
      inlet.water + sum(outlet.water) = 0 "flow balance";
      for i in 1:n loop
        outlet[i].T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
      end for;
      sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
      annotation(
        defaultComponentName = "pipe");
    end ConstantConvSIMO;

    model ConstantConvMISO "Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12. The inlet temperature is the weighted average of the incoming flows."
      //inlets
      extends ConstantConvection.BasePipeUA;
      parameter Integer n = 1 "Number of ports";
      WaterHub.BaseClasses.WaterPorts_in[n] inlet(each water(min = -1e-5)) annotation(
        Placement(transformation(extent = {{-110, -20}, {-90, 20}})));
      //outlets
      WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
        Placement(transformation(extent = {{110, -10}, {90, 10}})));
      WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
        Placement(transformation(extent = {{-10, -45}, {10, -25}})));
      //parameters
    equation
      for i in 1:nbNodes loop
        losses[i] = UA[i] * (nodeT[i] - tEnvironment);
// energy balance for each node.
        if i == 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (inlet.T .- nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        elseif i <> 1 then
          volume[i] * der(nodeT[i]) = sum(inlet.water) * (nodeT[i - 1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        end if;
      end for;
      sum(inlet.water) + outlet.water = 0 "flow balance";
      outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
      sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
      annotation(
        defaultComponentName = "pipe");
    end ConstantConvMISO;

    model ConstantConv_3EflowSISO "Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
      extends ConstantConvection.BasePipeUA;
      //inlets
      WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
        Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
      //outlets
      WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
        Placement(transformation(extent = {{110, -10}, {90, 10}})));
      WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
        Placement(transformation(extent = {{-10, -45}, {10, -25}})));
      //parameters
      // parameter SI.HeatFlow lossAtRest = 30;
      SI.HeatFlow lossAtRest[nbNodes];
      //dynamic losses during standby-mode.
      Boolean event(start = false);
      Boolean waterNeeded[nbNodes];
      parameter Real nodeFillTime[nbNodes] = fill(0.0, nbNodes);
      parameter Real waitingTime = 180 "waiting time before emptying the pipes";
      final constant Real triggerValue = 0.001;
      discrete Real t(start = 0.0);
    equation
      event = inlet.water > triggerValue "Careful, as a too high triggerValue leads to Chattering problems";
      when change(event) then
        t = time;
      end when;
      for i in 1:nbNodes loop
// energy balance for each node.
        lossAtRest[i] = UA[i] / 10 * (nodeT[i] - tEnvironment);
//reduce 10-fold the losses during stagnations phases.
        waterNeeded[i] = event or not event and time <= t + waitingTime + sum(nodeFillTime[1:i]);
        losses[i] = if waterNeeded[i] then UA[i] * (nodeT[i] - tEnvironment) else lossAtRest[i];
//waterNeeded is true or false depending on which "losses" apply (usual water flow or 0 losses in intermediate storage).
        if i == 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (inlet.T - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        elseif i <> 1 then
          volume[i] * der(nodeT[i]) = inlet.water * (nodeT[i - 1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        end if;
      end for;
      inlet.water + outlet.water = 0 "flow balance";
      outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
      sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
      annotation(
        defaultComponentName = "Pipe3Eflow");
    end ConstantConv_3EflowSISO;
    annotation(
      Documentation(info = "<html>
<p>The water pipe models contained in this package are all modeled using the same method. They were the first to be developed and hence are simpler than other models in the <span style=\"font-family: Courier New;\">WaterPipes</span> package.</p>
<p>A constant overall heat transfer coefficient UA is computed according to the formula</p>
<p><br>...</p>
<p><br>using constant convective heat transfer coefficients.</p>
</html>"));
  end ConstantConvection;

  model ClassicPipeThermalInertiaSISO "This time with thermal inertia: Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
    extends BasePipe;
    SI.AbsoluteTemperature wallNodeT[nbNodes](each start = tEnvironment);
    //inlets
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //parameters
  equation
    for i in 1:nbNodes loop
      losses[i] = CO.hConvAir * areaOutside[i] * (wallNodeT[i] - tEnvironment);
// energy balances for each node.
      if i == 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (inlet.T - nodeT[i]) - CO.hConvWater * areaInside[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      elseif i <> 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (nodeT[i - 1] - nodeT[i]) - CO.hConvWater * areaInside[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      end if;
      CWall * wallVolume[i] * der(wallNodeT[i]) = CO.hConvWater * areaInside[i] * (nodeT[i] - wallNodeT[i]) - losses[i];
//Wall to Environment
    end for;
    inlet.water + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
    annotation(
      defaultComponentName = "ClassicPipe");
  end ClassicPipeThermalInertiaSISO;

  model ClassicPipeThermalInertiaUASISO "This time with thermal inertia: Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
    extends ConstantConvection.BasePipeUA;
    SI.AbsoluteTemperature wallNodeT[nbNodes](each start = tEnvironment);
    //inlets
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //parameters
  equation
    for i in 1:nbNodes loop
      losses[i] = UA[i] * (wallNodeT[i] - tEnvironment);
// energy balances for each node.
      if i == 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (inlet.T - nodeT[i]) - UA[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      elseif i <> 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (nodeT[i - 1] - nodeT[i]) - UA[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      end if;
      CWall * wallVolume[i] * der(wallNodeT[i]) = UA[i] * (nodeT[i] - wallNodeT[i]) - losses[i];
//Wall to Environment
    end for;
    inlet.water + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
    annotation(
      defaultComponentName = "ClassicPipe");
  end ClassicPipeThermalInertiaUASISO;

  model Pipe3EflowThermalInertiaSISO "Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
    extends BasePipe;
    SI.AbsoluteTemperature wallNodeT[nbNodes](each start = tEnvironment);
    //inlets
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //parameters
    // parameter SI.HeatFlow lossAtRest = 30;
    SI.HeatFlow usefulUA[nbNodes];
    //dynamic losses during standby-mode.
    Boolean event(start = false);
    Boolean waterNeeded[nbNodes];
    parameter Real nodeFillTime[nbNodes] = fill(0.0, nbNodes);
    parameter Real waitingTime = 180 "waiting time before emptying the pipes";
    final constant Real triggerValue = 0.001;
    discrete Real t(start = 0.0);
    SI.HeatFlow losses[nbNodes];
  equation
    event = inlet.water > triggerValue "Careful, as a too high triggerValue leads to Chattering problems";
    when change(event) then
      t = time;
    end when;
    
    for i in 1:nbNodes loop
      hConv[i] = 0.0396 * (abs(inlet.water) * BF.waterThermalConductivity(nodeT[i]) / (areaCrossSection * BF.waterDynamicViscosity(nodeT[i]))) ^ 0.75 * BF.waterPrandtlNumber(nodeT[i]) ^ (1 / 3);
      hWater[i] = smooth(0, noEvent(if (hConv[i] > staticConv) then hConv[i] else staticConv));
      if insulationLayer then
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + log((pipeDiameterOutside + 2 * insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      else
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      end if;
  
// energy balances for each node.
//reduce 10-fold the losses during stagnations phases.
      waterNeeded[i] = event or not event and time <= t + waitingTime + sum(nodeFillTime[1:i]);
      usefulUA[i] = if waterNeeded[i] then UA[i] else UA[i] / 10;
//waterNeeded is true or false depending on which "losses" apply (usual water flow or 0 losses in intermediate storage).
      losses[i] = UA[i] * (wallNodeT[i] - tEnvironment);
      if i == 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (inlet.T - nodeT[i]) - usefulUA[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      elseif i <> 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (nodeT[i - 1] - nodeT[i]) - usefulUA[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      end if;
      CWall * wallVolume[i] * der(wallNodeT[i]) = usefulUA[i] * (nodeT[i] - wallNodeT[i]) - UA[i] * (wallNodeT[i] - tEnvironment);
//Wall to Environment
    end for;
    inlet.water + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
    annotation(
      defaultComponentName = "Pipe3Eflow");
  end Pipe3EflowThermalInertiaSISO;

  model PipeStVenant "STILL UNDER DEVELOPMENT -- Modeling the flow of water through a prismatic rectangular channel using the 1D St Venant equations"
    extends BasePipe;
    parameter Integer n = 1 "Number of ports";
    WaterHub.BaseClasses.WaterPorts_in[n] inlet(each water(min = -1e-5)) annotation(
      Placement(transformation(extent = {{-110, -20}, {-90, 20}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //parameters
    //SI.Area[nbNodes] A(each start = 0.0) "Cross section area";
    SI.WaterFlow[nbNodes] Q(each start = 0.0) "Water flow";
    SI.Height[nbNodes] h(each start = 0.0) "Water height";
    SI.Area P[nbNodes] "Wetted perimeter";
    final parameter SI.Length dx = pipeLength / (nbNodes - 1) "Delta x";
    parameter SI.Length b = 0.1 "width of channel";
    SI.Velocity[nbNodes] v(each start = 1.0) "mean velocity of water flow";
    //parameter Real Is = 0.002 "Slope of pipe";
    parameter SI.Length pipeFloorHeightDifference = 1;
    parameter Real kst = 85 "Hydraulic resistance";
    Real[nbNodes] Ie(each start = 0.0);
    SI.VolumeFlow fakeInlet;
    //  SI.OverallConductance UAEffective[nbNodes] "UA dependent on wetted area";
    //  algorithm
    //    UAEffective := UA * (P/(CO.pi*pipeDiameterInside));
  equation
    fakeInlet = sum(inlet.water);
// IN TERMS OF VELOCITY AND HEIGHT
    for i in 1:nbNodes loop
      losses[i] = UA[i] * (P[i] / (CO.pi * pipeDiameterInside)) * (nodeT[i] - tEnvironment);
// energy balance for each node.
//    A[i] = (pipeDiameterInside/2)^2 * acos((pipeDiameterInside/2-h[i])/(pipeDiameterInside/2)) - sqrt((pipeDiameterInside/2)^2 - ((pipeDiameterInside/2)-h[i])^2)*((pipeDiameterInside/2)-h[i]) "cross section area as a function of water height"; // ONLY FOR ROUND PIPE
//A[i] = h[i]*b "Cross sectional area of flowing water";
      P[i] = b + 2 * h[i] "Wetted perimeter of rectangular channel";
      Ie[i] = (v[i] / kst * (h[i] * b / P[i]) ^ (2 / 3)) ^ 2;
      Q[i] = h[i] * b * v[i];
      if i == 1 then
        der(nodeT[i]) = 0;
//if Q[i] > 0.0001 then (inlet.water * (inlet.T .- nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater)/Q[i] else 0;
        h[i] = fakeInlet / (v[i] * b);
        v[i] = 1;
//    elseif i == nbNodes then
//      h[i] = 0;
      else
        der(nodeT[i]) = 0;
//Q[i] * der(nodeT[i]) = sum(inlet.water) * (nodeT[i-1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
        der(h[i]) + (h[i] * v[i] - h[i - 1] * v[i - 1]) / dx = 0;
        der(v[i]) + (v[i] * v[i] - v[i - 1] * v[i - 1]) / (2 * dx) + CO.g_n * ((h[i] - h[i - 1]) / dx) = CO.g_n * h[i] * (Is - Ie[i]);
      end if;
    end for;
    Q[nbNodes] + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
  end PipeStVenant;

  //PACKAGE EXAMPLES

  package Examples
    extends Modelica.Icons.ExamplesPackage;

    model WaterPipesSISO
      extends Modelica.Icons.Example;
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 4) annotation(
        Placement(visible = true, transformation(origin = {-114, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.EnergySupplySO energySupplySO1 annotation(
        Placement(visible = true, transformation(origin = {-112, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile1(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/Shower_tmp.csv") annotation(
        Placement(visible = true, transformation(origin = {16, 72}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO ConstantConvPipe(airConv = 10, insulationLayer = true, insulationThickness = 0.00, kInsulation = 0.036, material = WaterHub.Constants.MaterialConstants.Material.Copper, nbNodes = 10, pipeDiameterInside = 0.010922, pipeLength = 30.5, pipeThickness = 0.001778, tEnvironment = 273.15 + 20) annotation(
        Placement(visible = true, transformation(origin = {-24, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.HeatSinkMI heatSinkMI1(n = 3) annotation(
        Placement(visible = true, transformation(origin = {-24, -112}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      WaterHub.DHWTechnologies.Boilers.InstantaneousBoiler InstantaneousBoiler(n = 3, setPointT = 57 + 273.15) annotation(
        Placement(visible = true, transformation(origin = {-70, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile2(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/Shower_tmp.csv") annotation(
        Placement(visible = true, transformation(origin = {16, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1(n = 3) annotation(
        Placement(visible = true, transformation(origin = {130, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile3(fileName = "C:/Users/Prabhat Joshi/polybox/Summer work/WaterHub/Examples/TestInputs/Shower_tmp.csv") annotation(
        Placement(visible = true, transformation(origin = {14, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Appliances.Showers.ClassicShower Shower2 annotation(
        Placement(visible = true, transformation(origin = {30, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Appliances.Showers.ClassicShower Shower annotation(
        Placement(visible = true, transformation(origin = {30, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Appliances.Showers.ClassicShower Shower1 annotation(
        Placement(visible = true, transformation(origin = {30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.Pipe pipe(airConv = 10, insulationLayer = true, insulationThickness = 0.00, kInsulation = 0.036, nbNodes = 10, pipeDiameterInside = 0.010922, pipeLength = 30.5, pipeThickness = 0.001778, tEnvironment = 273.15 + 20) annotation(
        Placement(visible = true, transformation(origin = {-24, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.PipeThermalInertia pipeThermalInertia(airConv = 10, insulationLayer = true, insulationThickness = 0.00, kInsulation = 0.036, nbNodes = 10, pipeDiameterInside = 0.010922, pipeLength = 30.5, pipeThickness = 0.001778, tEnvironment = 273.15 + 20) annotation(
        Placement(visible = true, transformation(origin = {-24, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(pipeThermalInertia.heatOutlet, heatSinkMI1.inlet[2]) annotation(
        Line(points = {{-24, -11.5}, {-24, -11.5}, {-24, -102}, {-24, -102}}, color = {208, 52, 5}));
      connect(pipeThermalInertia.outlet, Shower1.inletHot) annotation(
        Line(points = {{-14, -8}, {20, -8}, {20, -8}, {20, -8}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[4], Shower2.inletCold) annotation(
        Line(points = {{-104, -21}, {-4, -21}, {-4, -72}, {20, -72}, {20, -72}}, color = {0, 127, 255}));
      connect(ConstantConvPipe.heatOutlet, heatSinkMI1.inlet[1]) annotation(
        Line(points = {{-24, 48.5}, {-24, -26}, {-24, -102}, {-26.6667, -102}}, color = {208, 52, 5}));
      connect(pipe.heatOutlet, heatSinkMI1.inlet[3]) annotation(
        Line(points = {{-24, -71.5}, {-24, -86}, {-24, -102}, {-21.3333, -102}}, color = {208, 52, 5}));
      connect(hydrographFromFile3.y, Shower2.flowInput) annotation(
        Line(points = {{25, -48}, {31, -48}, {31, -52}, {30, -52}, {30, -60.4}}, color = {0, 0, 127}, thickness = 0.5));
      connect(pipe.outlet, Shower2.inletHot) annotation(
        Line(points = {{-14, -68}, {20, -68}}, color = {0, 127, 255}));
      connect(Shower1.outlet, waterSinkMI1.inlet[2]) annotation(
        Line(points = {{40, -10}, {120, -10}, {120, -10}, {120, -10}}, color = {0, 127, 255}));
      connect(hydrographFromFile2.y, Shower1.flowInput) annotation(
        Line(points = {{27, 12}, {30, 12}, {30, -0.4}, {30, -0.4}}, color = {0, 0, 127}, thickness = 0.5));
      connect(waterSupplyMO1.outlets[3], Shower1.inletCold) annotation(
        Line(points = {{-104, -23}, {-4, -23}, {-4, -12}, {20, -12}, {20, -12}}, color = {0, 127, 255}));
      connect(Shower.outlet, waterSinkMI1.inlet[1]) annotation(
        Line(points = {{40, 50}, {66, 50}, {66, -10}, {120, -10}, {120, -12.6667}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], Shower.inletCold) annotation(
        Line(points = {{-104, -25}, {-4, -25}, {-4, 48}, {20, 48}, {20, 48}}, color = {0, 127, 255}));
      connect(ConstantConvPipe.outlet, Shower.inletHot) annotation(
        Line(points = {{-14, 52}, {20, 52}}, color = {0, 127, 255}));
      connect(hydrographFromFile1.y, Shower.flowInput) annotation(
        Line(points = {{27, 72}, {30, 72}, {30, 59.6}}, color = {0, 0, 127}, thickness = 0.5));
      connect(Shower2.outlet, waterSinkMI1.inlet[3]) annotation(
        Line(points = {{40, -70}, {66, -70}, {66, -10}, {120, -10}, {120, -7.33333}}, color = {0, 127, 255}));
      connect(InstantaneousBoiler.outlet[1], ConstantConvPipe.inlet) annotation(
        Line(points = {{-60, 5.33333}, {-52, 5.33333}, {-52, 52}, {-34, 52}, {-34, 52}}, color = {0, 127, 255}));
      connect(energySupplySO1.outlet, InstantaneousBoiler.energy_in) annotation(
        Line(points = {{-102, 34}, {-70, 34}, {-70, 18}, {-70, 18}}, color = {208, 52, 5}));
      connect(waterSupplyMO1.outlets[1], InstantaneousBoiler.inlet) annotation(
        Line(points = {{-104, -27}, {-96, -27}, {-96, 8}, {-80, 8}, {-80, 8}}, color = {0, 127, 255}));
      connect(InstantaneousBoiler.outlet[2], pipeThermalInertia.inlet) annotation(
        Line(points = {{-60, 8}, {-52, 8}, {-52, -8}, {-34, -8}}, color = {0, 127, 255}));
      connect(InstantaneousBoiler.outlet[3], pipe.inlet) annotation(
        Line(points = {{-60, 10.6667}, {-52, 10.6667}, {-52, -68}, {-34, -68}}, color = {0, 127, 255}));
    end WaterPipesSISO;

    model WaterPipesFlowModyfyingAppliances
      extends Modelica.Icons.Example;
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 4) annotation(
        Placement(visible = true, transformation(origin = {-114, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Imports.EnergySupplySO energySupplySO1 annotation(
        Placement(visible = true, transformation(origin = {-112, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile1(columns = {2, 3, 4}, fileName = "C:/Users/hadengbr/polybox/EAWAG/05_Raw_Data/05_06_Validation/181016_Validation3Eflow/tmp/181016_Validation3Eflow_M.csv") annotation(
        Placement(visible = true, transformation(origin = {16, 72}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.ConstantConvection.ConstantConvSISO ClassicPipe(material = WaterHub.Constants.MaterialConstants.Material.PEX, nbNodes = 10) annotation(
        Placement(visible = true, transformation(origin = {-24, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.HeatSinkMI heatSinkMI1(n = 6) annotation(
        Placement(visible = true, transformation(origin = {-24, -116}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      WaterHub.DHWTechnologies.Boilers.InstantaneousBoiler InstantaneousBoiler(n = 3) annotation(
        Placement(visible = true, transformation(origin = {-70, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile2(columns = {2, 3, 4}, fileName = "C:/Users/hadengbr/polybox/EAWAG/05_Raw_Data/05_06_Validation/181016_Validation3Eflow/tmp/181016_Validation3Eflow_M.csv") annotation(
        Placement(visible = true, transformation(origin = {16, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1(n = 3) annotation(
        Placement(visible = true, transformation(origin = {130, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Blocks.Sources.hydrographFromFile hydrographFromFile3(columns = {2, 3, 4}, fileName = "C:/Users/hadengbr/polybox/EAWAG/05_Raw_Data/05_06_Validation/181016_Validation3Eflow/tmp/181016_Validation3Eflow_M.csv") annotation(
        Placement(visible = true, transformation(origin = {16, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Taps.FlowModifyingTap FlowModifyingTap annotation(
        Placement(visible = true, transformation(origin = {30, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Taps.FlowModifyingTap FlowModifyingTap1 annotation(
        Placement(visible = true, transformation(origin = {30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.Taps.FlowModifyingTap FlowModifyingTap2 annotation(
        Placement(visible = true, transformation(origin = {30, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.ClassicPipeThermalInertiaUASISO ClassicPipeUA(material = WaterHub.Constants.MaterialConstants.Material.PEX, nbNodes = 10) annotation(
        Placement(visible = true, transformation(origin = {-24, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.Pipe3EflowThermalInertiaSISO Pipe3EflowUA annotation(
        Placement(visible = true, transformation(origin = {-24, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(InstantaneousBoiler.outlet[2], Pipe3EflowUA.inlet) annotation(
        Line(points = {{-60, 8}, {-52, 8}, {-52, -8}, {-34, -8}, {-34, -8}}, color = {0, 127, 255}));
      connect(Pipe3EflowUA.heatOutlet, heatSinkMI1.inlet[2]) annotation(
        Line(points = {{-24, -12}, {-24, -12}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(Pipe3EflowUA.outlet, FlowModifyingTap1.inletHot) annotation(
        Line(points = {{-14, -8}, {20, -8}, {20, -8}, {20, -8}}, color = {0, 127, 255}));
      connect(ClassicPipeUA.heatOutlet, heatSinkMI1.inlet[3]) annotation(
        Line(points = {{-24, -72}, {-24, -72}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(ClassicPipeUA.outlet, FlowModifyingTap2.inletHot) annotation(
        Line(points = {{-14, -68}, {20, -68}, {20, -68}, {20, -68}}, color = {0, 127, 255}));
      connect(InstantaneousBoiler.outlet[3], ClassicPipeUA.inlet) annotation(
        Line(points = {{-60, 8}, {-52, 8}, {-52, -68}, {-34, -68}, {-34, -68}, {-34, -68}}, color = {0, 127, 255}));
      connect(FlowModifyingTap2.heatOutlet, heatSinkMI1.inlet[6]) annotation(
        Line(points = {{30, -80}, {30, -80}, {30, -92}, {-24, -92}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(FlowModifyingTap1.heatOutlet, heatSinkMI1.inlet[5]) annotation(
        Line(points = {{30, -20}, {30, -20}, {30, -30}, {60, -30}, {60, -92}, {-24, -92}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(FlowModifyingTap.heatOutlet, heatSinkMI1.inlet[4]) annotation(
        Line(points = {{30, 40}, {30, 40}, {30, 30}, {60, 30}, {60, -92}, {-24, -92}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(ClassicPipe.heatOutlet, heatSinkMI1.inlet[1]) annotation(
        Line(points = {{-24, 48}, {-24, 48}, {-24, -106}, {-24, -106}}, color = {208, 52, 5}));
      connect(hydrographFromFile3.y, FlowModifyingTap2.flowInput) annotation(
        Line(points = {{28, -48}, {30, -48}, {30, -60}, {30, -60}, {30, -60}}, color = {0, 0, 127}, thickness = 0.5));
      connect(hydrographFromFile2.y, FlowModifyingTap1.flowInput) annotation(
        Line(points = {{28, 12}, {30, 12}, {30, 0}, {30, 0}}, color = {0, 0, 127}, thickness = 0.5));
      connect(hydrographFromFile1.y, FlowModifyingTap.flowInput) annotation(
        Line(points = {{28, 72}, {30, 72}, {30, 60}, {30, 60}}, color = {0, 0, 127}, thickness = 0.5));
      connect(FlowModifyingTap2.outlet, waterSinkMI1.inlet[3]) annotation(
        Line(points = {{40, -70}, {80, -70}, {80, -10}, {120, -10}, {120, -10}}, color = {0, 127, 255}));
      connect(FlowModifyingTap1.outlet, waterSinkMI1.inlet[2]) annotation(
        Line(points = {{40, -10}, {120, -10}, {120, -10}, {120, -10}}, color = {0, 127, 255}));
      connect(FlowModifyingTap.outlet, waterSinkMI1.inlet[1]) annotation(
        Line(points = {{40, 50}, {80, 50}, {80, -10}, {120, -10}, {120, -10}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[4], FlowModifyingTap2.inletCold) annotation(
        Line(points = {{-104, -24}, {-4, -24}, {-4, -72}, {20, -72}, {20, -72}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[3], FlowModifyingTap1.inletCold) annotation(
        Line(points = {{-104, -24}, {-4, -24}, {-4, -12}, {20, -12}, {20, -12}}, color = {0, 127, 255}));
      connect(waterSupplyMO1.outlets[2], FlowModifyingTap.inletCold) annotation(
        Line(points = {{-104, -24}, {-4, -24}, {-4, 48}, {20, 48}, {20, 48}}, color = {0, 127, 255}));
      connect(ClassicPipe.outlet, FlowModifyingTap.inletHot) annotation(
        Line(points = {{-14, 52}, {20, 52}, {20, 52}, {20, 52}}, color = {0, 127, 255}));
      connect(InstantaneousBoiler.outlet[1], ClassicPipe.inlet) annotation(
        Line(points = {{-60, 8}, {-52, 8}, {-52, 52}, {-34, 52}, {-34, 52}}, color = {0, 127, 255}));
      connect(energySupplySO1.outlet, InstantaneousBoiler.energy_in) annotation(
        Line(points = {{-102, 34}, {-70, 34}, {-70, 18}, {-70, 18}}, color = {208, 52, 5}));
      connect(waterSupplyMO1.outlets[1], InstantaneousBoiler.inlet) annotation(
        Line(points = {{-104, -24}, {-96, -24}, {-96, 8}, {-80, 8}, {-80, 8}}, color = {0, 127, 255}));
    end WaterPipesFlowModyfyingAppliances;

    model WaterPipe_StVenant
      extends Modelica.Icons.Example;
      WaterHub.ImportExport.Imports.WaterSupplyMO waterSupplyMO1(n = 1) annotation(
        Placement(visible = true, transformation(origin = {-114, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Blocks.Sources.hydrographFromFile hydrographFromFile2(fileName = Modelica.Utilities.Files.loadResource("modelica://WaterHub/Examples/TestInputs/Toilet_tmp.csv")) annotation(
        Placement(visible = true, transformation(origin = {-58, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.ImportExport.Exports.WaterSinkMI waterSinkMI1(n = 1) annotation(
        Placement(visible = true, transformation(origin = {130, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Appliances.ColdPlumbed.Toilet toilet1 annotation(
        Placement(visible = true, transformation(origin = {-36, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      WaterHub.Conveyance.WaterPipes.PipeStVenant pipeStVenant1(Is = 0.1, b = 0.5, nbNodes = 1000, pipeDiameterInside = 0.1, pipeLength = 1) annotation(
        Placement(visible = true, transformation(origin = {42, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ImportExport.Exports.HeatSinkMI heatSinkMI1 annotation(
        Placement(visible = true, transformation(origin = {78, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(waterSupplyMO1.outlets[1], toilet1.inlet) annotation(
        Line(points = {{-104, -8}, {-46, -8}}, color = {0, 127, 255}));
      connect(hydrographFromFile2.y, toilet1.flowInput) annotation(
        Line(points = {{-46, 18}, {-36, 18}, {-36, 4}}, color = {0, 0, 127}, thickness = 0.5));
      connect(toilet1.outlet, pipeStVenant1.inlet[1]) annotation(
        Line(points = {{-26, -6}, {32, -6}}, color = {0, 127, 255}));
      connect(pipeStVenant1.outlet, waterSinkMI1.inlet[1]) annotation(
        Line(points = {{52, -6}, {120, -6}}, color = {0, 127, 255}));
      connect(pipeStVenant1.heatOutlet, heatSinkMI1.inlet[1]) annotation(
        Line(points = {{42, -9.5}, {42, -38}, {68, -38}}, color = {208, 52, 5}));
    end WaterPipe_StVenant;
  end Examples;

  partial model BasePipe "Model inspired by Hanby, V. I., Wright, J. A., Fletcher, D. . and Jones, D. N. . (2002) ‘Modeling the dynamic response of conduits’, HVAC & R Research, 8(1), pp. 1–12."
    parameter Integer nbNodes = 10 "Number of nodes";
    parameter SI.PipeLength pipeLength = 29;
    parameter SI.PipeDiameter pipeDiameterInside = 0.0127;
    parameter SI.PipeThickness pipeThickness = 0.00375;
    final parameter SI.PipeDiameter pipeDiameterOutside = pipeDiameterInside + 2 * pipeThickness;
    parameter CO.MaterialConstants.Material material = CO.MaterialConstants.Material.Copper;
    parameter SI.AbsoluteTemperature tEnvironment = 293.15;
    parameter Boolean insulationLayer = false annotation(
      Dialog(group = "Insulation"));
    //Insulation parameters
    parameter SI.PipeThickness insulationThickness = 0.0 annotation(
      Dialog(group = "Insulation", enable = insulationLayer));
    parameter SI.ThermalConductivity kInsulation = 0.004 annotation(
      Dialog(group = "Insulation", enable = insulationLayer));
    //  parameter SI.Density rhoInsulation = 70 annotation(
    //    Dialog(group = "Insulation", enable = insulationLayer));
    //  parameter SI.SpecificHeatCap CInsulation = 1045.0 annotation(
    //    Dialog(group = "Insulation", enable = insulationLayer));
    //  final parameter SI.VolSpecificHeatCap CInsulationVol = rhoInsulation * CInsulation  annotation(
    //    Dialog(group = "Insulation", enable = InsulationLayer));
    //variables
    SI.Volume volume[nbNodes];
    SI.Volume wallVolume[nbNodes];
    SI.Volume insulationVolume[nbNodes];
    SI.AbsoluteTemperature nodeT[nbNodes](each start = tEnvironment);
    SI.Area areaInside[nbNodes];
    SI.Area areaOutside[nbNodes];
    SI.Area areaCrossSection;
    SI.ThermalConductivity k;
    SI.Density wallDensity;
    SI.VolSpecificHeatCap CWall;
    SI.OverallConductance UA[nbNodes];
    SI.hConv hConv[nbNodes] "Convection coefficient water / pipe wall";
    SI.CoefficientOfHeatTransfer hWater[nbNodes] "Heat transfer coefficient water / pipe wall";
    //parameters
    parameter SI.hConv staticConv = 100;
    parameter SI.hConv airConv = 30;
  algorithm
    if noEvent(material == CO.MaterialConstants.Material.Copper) then
      k := 400.0 "Values for k are taken from https://www.engineeringtoolbox.com/thermal-conductivity-d_429.html";
      wallDensity := 8940;
      CWall := wallDensity * 390 "Density (kg m-3) times specific heat capacity (J kg-1 K-1)";
    elseif material == CO.MaterialConstants.Material.PEX then
      k := 0.50;
      wallDensity := 940;
      CWall := wallDensity * 550 "Density (kg m-3) times specific heat capacity (J kg-1 K-1)";
    elseif material == CO.MaterialConstants.Material.Steel then
      k := 16;
      wallDensity := 7850;
      CWall := wallDensity * 502.4 "Density (kg m-3) times specific heat capacity (J kg-1 K-1)";
    else
      k := 0.0;
      wallDensity := 0;
      CWall := 0.0;
    end if;
    areaCrossSection := CO.pi * (pipeDiameterInside / 2) ^ 2;
    for i in 1:nbNodes loop
      volume[i] := pipeLength / nbNodes * CO.pi * (pipeDiameterInside / 2) ^ 2 * 1e3 "Volume of each node in Liters. The pipe is divided into nbNodes equal sections";
      wallVolume[i] := pipeLength / nbNodes * CO.pi * (pipeDiameterOutside ^ 2 - pipeDiameterInside ^ 2) / 4;
      areaInside[i] := CO.pi * pipeDiameterInside * (pipeLength / nbNodes);
      if insulationLayer then
        areaOutside[i] := CO.pi * (pipeDiameterOutside + 2 * insulationThickness) * (pipeLength / nbNodes);
      else
        areaOutside[i] := CO.pi * pipeDiameterOutside * (pipeLength / nbNodes);
      end if;
      insulationVolume[i] := pipeLength / nbNodes * CO.pi * ((pipeDiameterOutside / 2 + insulationThickness) ^ 2 - (pipeDiameterOutside / 2) ^ 2);
    end for;
  equation

    annotation(
      Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}, fillPattern = FillPattern.Solid, fillColor = {95, 95, 95}, pattern = LinePattern.None), Rectangle(extent = {{-100, 44}, {100, -44}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 127, 255}), Text(extent = {{-90.0, -20.0}, {90.0, 20.0}}, lineColor = {255, 255, 255}, textString = "%name")}));
  end BasePipe;

  model Pipe
    extends WaterHub.Conveyance.WaterPipes.BasePipe;
    //inlets
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-3)) annotation(
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-3)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //variables
    SI.HeatFlow losses[nbNodes];
  
  equation
    for i in 1:nbNodes loop
      hConv[i] = 0.0396 * (abs(inlet.water) * BF.waterThermalConductivity(nodeT[i]) / (areaCrossSection * BF.waterDynamicViscosity(nodeT[i]))) ^ 0.75 * BF.waterPrandtlNumber(nodeT[i]) ^ (1 / 3);
      hWater[i] = smooth(0, noEvent(if (hConv[i] > staticConv) then hConv[i] else staticConv));
      if insulationLayer then
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + log((pipeDiameterOutside + 2 * insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      else
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      end if;
      losses[i] = UA[i] * (nodeT[i] - tEnvironment);
      if i == 1 then
        volume[i] * der(nodeT[i]) = inlet.water * (inlet.T - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
      elseif i <> 1 then
        volume[i] * der(nodeT[i]) = inlet.water * (nodeT[i - 1] - nodeT[i]) - losses[i] / CO.VolSpecificHeatCapWater;
      end if;
    end for;
    inlet.water + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
    annotation(
      defaultComponentName = "pipe");
  end Pipe;

  model PipeThermalInertia
    extends WaterHub.Conveyance.WaterPipes.BasePipe;
    SI.AbsoluteTemperature wallNodeT[nbNodes](each start = tEnvironment);
    //inlets
    WaterHub.BaseClasses.WaterPort_in inlet(water(min = -1e-5)) annotation(
      Placement(transformation(extent = {{-110, -10}, {-90, 10}})));
    //outlets
    WaterHub.BaseClasses.WaterPort_out outlet(water(max = 1e-5)) annotation(
      Placement(transformation(extent = {{110, -10}, {90, 10}})));
    WaterHub.BaseClasses.HeatPort_out heatOutlet "to be connected to heat sink" annotation(
      Placement(transformation(extent = {{-10, -45}, {10, -25}})));
    //variables
    SI.HeatFlow losses[nbNodes];
    SI.OverallConductance transferCoeff[nbNodes];
  equation
    for i in 1:nbNodes loop
      hConv[i] = 0.0396 * (abs(inlet.water) * BF.waterThermalConductivity(nodeT[i]) / (areaCrossSection * BF.waterDynamicViscosity(nodeT[i]))) ^ 0.75 * BF.waterPrandtlNumber(nodeT[i]) ^ (1 / 3);
      hWater[i] = smooth(0, noEvent(if hConv[i] > staticConv then hConv[i] else staticConv));
      if insulationLayer then
        transferCoeff[i] = 1. / (log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + log((pipeDiameterOutside + 2 * insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + log((pipeDiameterOutside + 2 * insulationThickness) / pipeDiameterOutside) / (kInsulation * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      else
        transferCoeff[i] = 1. / (log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
        UA[i] = 1. / (1. / (hWater[i] * areaInside[i]) + log(pipeDiameterOutside / pipeDiameterInside) / (k * 2 * CO.pi * pipeLength / nbNodes) + 1. / (airConv * areaOutside[i]));
      end if;
      losses[i] = transferCoeff[i] * (wallNodeT[i] - tEnvironment);
// energy balances for each node.
      if i == 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (inlet.T - nodeT[i]) - hWater[i] * areaInside[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      elseif i <> 1 then
        CO.VolSpecificHeatCapWater * volume[i] * der(nodeT[i]) = inlet.water * CO.VolSpecificHeatCapWater * (nodeT[i - 1] - nodeT[i]) - hWater[i] * areaInside[i] * (nodeT[i] - wallNodeT[i]);
// Fluid 2 Wall
      end if;
      CWall * wallVolume[i] * der(wallNodeT[i]) = hWater[i] * areaInside[i] * (nodeT[i] - wallNodeT[i]) - losses[i];
//Wall to Environment
    end for;
    inlet.water + outlet.water = 0 "flow balance";
    outlet.T = nodeT[nbNodes] "The outlet temperature is equal to the temperature of the last node";
    sum(losses) + heatOutlet.heat = 0 "Sum of heat lost across the pipe wall";
    annotation(
      defaultComponentName = "ClassicPipe");
  end PipeThermalInertia;
end WaterPipes;
